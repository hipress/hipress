from asyncio import start_server
from genericpath import isdir
import os

def get_files(path, loop=True):
    files = []
    if loop:        
        # get all files in the path recursively
        for root, dirs, file_names in os.walk(path):
            for file_name in file_names:
                files.append(os.path.join(root, file_name))
    else:
        # only get files in the path
        for file_name in os.listdir(path):
            file_name = os.path.join(path, file_name)
            if os.path.isdir(file_name):
                continue
            else:
                files.append(file_name)
    return files

def get_data(file_name):
    with open(file_name, 'r') as f:
        data = f.read()
        lines = data.splitlines()
    return lines

def get_key_index(line, key):
    left =  line.find(key)
    right = left + len(key)
    if left == -1:
        print(f"key:{key} not found in line:{line}")
    return left, right

path = './LOG/SIM'
file_key = ['512mb']
# data_key = ["INFO Epoch[0] Batch[100]"]
start_key = "INFO Epoch[0] Batch[40]" 
end_key = "INFO Epoch[0] Batch[140]" 

key_bucket = []
value_bucket = []

files = get_files(path, loop=False)
files.sort()

for file in files:
    print(file)
    data_lines = get_data(file)
    for line in data_lines:
        key_word = "_alltoall:"
        if key_word in line:
            left, right = get_key_index(line, key_word)
            key = line[left:right]
            value = line[right+1:-2]
            key_bucket.append(key)
            value_bucket.append(float(value))
# print("len(key_bucket):", len(value_bucket))
# print(value_bucket)

# for file in files:
#     if any(key in file for key in file_key):
#         print(file.split('_')[:3])
#         key_bucket.append(file.split('_')[:3])
#         data_lines = get_data(file)
#         sum = 0
#         count = 0
#         switch = False
#         for line in data_lines:
#             if start_key in line:
#                 switch = True
#             if switch:
#                 _, l = get_key_index(line, "Speed:")
#                 r, _ = get_key_index(line, "samples/sec")
#                 sum += float(line[l:r])
#                 count += 1
#             if end_key in line:
#                 print(f"average speed:{sum/count:.2f}")
#                 value_bucket.append(sum/count)
#                 break

# print(len(key_bucket), len(value_bucket))

# print(key_bucket)

c = 1
for v in value_bucket:
    if c % 4 == 0:
        print(f"{v:.2f}")
    else:
        print(f"{v:.2f}", end=" ")
    c += 1

        
            