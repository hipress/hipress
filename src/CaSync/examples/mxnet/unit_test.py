import horovod.mxnet as hvd
import mxnet as mx
from horovod.mxnet.mpi_ops import hipress_broadcast_, hipress_gather_, allreduce_, hipress_reduce_
import time

hvd.init()
rank = hvd.rank()
size = hvd.size()
local_rank = hvd.local_rank()
cross_size = hvd.cross_size()
dep = mx.nd.ones((1,), ctx=mx.cpu())
backend = "MPI"
N = 1100

def test_fusionOp():
    cpu_buffer = {}
    gpu_buffer = {}
    for i in range(size):
        cpu_buffer[i] = mx.nd.ones((1024*1024*10, ), ctx=mx.cpu(), dtype='uint8') * rank
        gpu_buffer[i] = mx.nd.ones((1024*1024*10, ), ctx=mx.gpu(local_rank), dtype='uint8') * rank
    for M in [10]:
        if backend == "NCCL":
            grad = gpu_buffer
            hipress_backend_level = 0
        elif backend == "MPI":
            grad = cpu_buffer 
            hipress_backend_level = 1
        for test_mode in ["FusionGather"]:
            for num in range(10):
                if backend == "MPI":
                    for i in range(size):
                        cpu_buffer[i][:M].copyto(gpu_buffer[i][:M])
                        gpu_buffer[i][:M].copyto(cpu_buffer[i][:M])
                if test_mode == "FusionGather" or test_mode == "FusionGatherAlltoall":
                    for i in range(1):
                        if test_mode == "FusionGather":
                            root_rank = 0
                        else:
                            root_rank = i
                        hipress_gather_(grad[i], dep, root_rank, name=str(i), num_elem=M, backend=hipress_backend_level)
                if test_mode == "FusionBCast" or test_mode == "FusionBCastAlltoall":
                    for i in range(size):  
                        if test_mode == "FusionBCast":
                            root_rank = 0
                        else:
                            root_rank = i
                        hipress_broadcast_(grad[i], root_rank, name=str(i), num_elem=M, backend=hipress_backend_level)
                
                for i in range(size):
                    grad[i].wait_to_read()
                    print("grad[", i, "]:", grad[i][:50])



def test_communicator():
    def hierachical_allreduce(gpu_tensor, i):
            local_root = 0 # local_root must be 0 in this version
            hipress_reduce_(gpu_tensor, local_root, name=str(i), num_elem=gpu_tensor.size)
            allreduce_(gpu_tensor, average=False, name=str(i), comm=2)
            gpu_tensor /= hvd.cross_size()
            hipress_broadcast_(gpu_tensor, local_root, name=str(i), num_elem=gpu_tensor.size, backend=0, comm=1)
    def global_allreduce(gpu_tensor, i):
        allreduce_(gpu_tensor, average=False, name=str(i), comm=0)
        gpu_tensor /= hvd.size()
        
    def compreesed_allreduce(gpu_tensor, buffer, i):
        local_root = 0 # local_root must be 0 in this version
        hipress_reduce_(gpu_tensor, local_root, name=str(i), num_elem=gpu_tensor.size)
        gpu_tensor.copyto(buffer[:tensor_KB])
        hipress_gather_(buffer, gpu_tensor, 0, name=str(i), num_elem=gpu_tensor.size, backend=1, comm=2)
        # for j in range(cross_size-2):
        #     buffer[:tensor_KB] += buffer[(j+1)*tensor_KB:(j+2)*tensor_KB]
        # buffer[:tensor_KB] /= cross_size
        hipress_broadcast_(buffer, 0, name=str(i), num_elem=gpu_tensor.size, backend=1, comm=2)
        buffer[:tensor_KB].copyto(gpu_tensor)
        hipress_broadcast_(gpu_tensor, local_root, name=str(i), num_elem=gpu_tensor.size, backend=0, comm=1)
        gpu_tensor.wait_to_read()
        print(gpu_tensor)
        
    tensor_KB = 10 #256 * 4 * 32 
    gpu_tensor = mx.nd.ones((tensor_KB , ), ctx=mx.gpu(local_rank)) * rank #Float32 = 4 bytes
    buffer = mx.nd.ones((tensor_KB * size, ), ctx=mx.cpu()) * rank #Float32 = 4 bytes
    # comm = 0:GLOBAL, 1:LOCAL, 2:CROSS
    start = time.time()
    for i in range(10):
        gpu_tensor += 1
        compreesed_allreduce(gpu_tensor, buffer, i)
        # if i % 99 == 0:
        #     gpu_tensor.wait_to_read()
        #     end = time.time()
        #     print(f"Communicator avg_cost_time={(end-start)*1000/100:.2f}ms")
        #     start = end
    
    
def CaSync(original_elements, cr, gpu_buffer, cpu_buffer, aggregation_factor):
    num_of_elements = int(original_elements * cr)
    gpu_buffer[:2*num_of_elements].copyto(cpu_buffer[:2*num_of_elements])
    hipress_gather_(cpu_buffer, gpu_buffer, 0, name="gather", num_elem=2*num_of_elements, backend=1, comm=0)
    cpu_buffer[:2*num_of_elements].copyto(gpu_buffer[:2*num_of_elements])
    # 解压缩压缩过程省略
    num_of_elements *= aggregation_factor
    gpu_buffer[:2*num_of_elements].copyto(cpu_buffer[:2*num_of_elements])
    hipress_broadcast_(cpu_buffer, 0, name="broadcast", num_elem=2*num_of_elements, backend=1, comm=0)
    cpu_buffer[:2*num_of_elements].copyto(gpu_buffer[:2*num_of_elements])
    
def Two_step_allreduce(original_elements, cr, gpu_buffer, cpu_buffer, aggregation_factor):
    # gpu_buffer[:num_of_elements].copyto(cpu_buffer[:num_of_elements])
    mask_size = int(original_elements/8/4)
    allreduce_(gpu_buffer[:mask_size], average=False, name="mask")
    value_size = int(original_elements*cr*aggregation_factor)
    allreduce_(gpu_buffer[:value_size], average=False, name="value")
    


def compare():
    original_elements = 4*1024*1024 #元素个数
    compress_ratio_list = [0.01, 0.1]
    aggregation_factor_list = [1, 2, 4, 8]
    gpu_buffer = mx.nd.ones((40, ), ctx=mx.gpu(local_rank)) * rank
    cpu_buffer = mx.nd.ones((128*1024*1024 , ), ctx=mx.cpu()) * rank 
    allreduce_(gpu_buffer, average=False, name="mask", prescale_factor=1.0 , num_elem=10)

    allreduce_(gpu_buffer, average=False, name="value", prescale_factor=1.0 , num_elem=20)
    print(gpu_buffer)
if __name__ == "__main__":
    compare()