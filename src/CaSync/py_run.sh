
if [ "${MODEL}" = "bert" ]; then
    cmd="python3 ./examples/mxnet/mxnet_bert.py \
        --batch-size ${BATCH_SIZE} \
        --num-epochs ${EPOCH} \
        --model ${MODEL} \
        --iteration ${ITERATION} \
        --log-interval 10 \
        --log-path ${LOG_PATH}
        --log-name ${SAVE_NAME} \
        --log-level ${LOG_LEVEL} \
        --comp-alg=${COMP_ALG} \
        --comp-threshold=${COMP_THRESHOLD} \
        --bert=${BERT} "
elif [ "${MODEL}" = "vgg19" ]; then
    cmd="python3 ./examples/mxnet/mxnet_imagenet_resnet50.py \
        --mode gluon \
        --batch-size ${BATCH_SIZE} \
        --num-epochs ${EPOCH} \
        --num-examples 51200 \
        --model ${MODEL} \
	    --image-shape 3,224,224 \
        --iteration ${ITERATION} \
        --log-interval 10 \
        --log-path ${LOG_PATH}
        --log-name ${SAVE_NAME} \
        --log-level ${LOG_LEVEL} \
        --comp-alg=${COMP_ALG} \
        --comp-threshold=${COMP_THRESHOLD} "
elif [ "${MODEL}" = "mlp" ]; then
    cmd="python3 ./examples/mxnet/mxnet_MLP.py \
        --mode gluon \
        --batch-size ${BATCH_SIZE} \
        --num-epochs ${EPOCH} \
        --num-examples 51200 \
        --model ${MODEL} \
	    --image-shape 8192 \
        --num-classes 8192 \
        --iteration ${ITERATION} \
        --log-interval 10 \
        --log-path ${LOG_PATH}
        --log-name ${SAVE_NAME} \
        --log-level ${LOG_LEVEL} \
        --comp-alg=${COMP_ALG} \
        --comp-threshold=${COMP_THRESHOLD} "
else 
    cmd="python3 ./examples/mxnet/unit_test.py"
fi 

# Real Dataset 
# --use-rec \
# --rec-train=/data/trainData/traindata.rec \
# --rec-train-idx=/data/trainData/traindata.idx \
# --rec-val="/data/trainData/imagenet1k-val.rec" \
# --rec-val-idx="/data/trainData/imagenet1k-val.idx" \

${cmd} > output_rank${OMPI_COMM_WORLD_RANK}.log 2>&1
