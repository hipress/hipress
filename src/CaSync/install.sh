pip uninstall -y horovod

# Install Horovod

# # Method 1: Install from sdist
# # step 1: build sdist
# rm -rf ./dist
# python3 setup.py sdist
# # step 2: install from sdist
# export HOROVOD_WITHOUT_TENSORFLOW=1 HOROVOD_WITH_PYTORCH=1 HOROVOD_WITHOUT_MXNET=1
# export HOROVOD_WITH_MPI=1 HOROVOD_MPI_HOME=/usr/local/mpi/
# export HOROVOD_WITH_NCCL=1 HOROVOD_NCCL_HOME=/usr/local/nccl/ 
# export HOROVOD_GPU_OPERATIONS=NCCL
# export HOROVOD_WITHOUT_GLOO=1
# pip install --no-cache-dir dist/horovod-0.20.3.tar.gz

# Method 2: Install from wheel
# rm -rf ./build
export HOROVOD_WITHOUT_TENSORFLOW=1 HOROVOD_WITH_PYTORCH=1 HOROVOD_WITHOUT_MXNET=1
export HOROVOD_WITH_MPI=1 HOROVOD_MPI_HOME=/usr/local/mpi/
export HOROVOD_WITH_NCCL=1 HOROVOD_NCCL_HOME=/usr/local/nccl/ 
export HOROVOD_GPU_OPERATIONS=NCCL
export HOROVOD_WITHOUT_GLOO=1
python3 setup.py install