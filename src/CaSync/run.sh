export GPUS_PER_NODE=4
export NODES=1

export HIPRESS_BACKEND=MPI # MPI or NCCL
export HIPRESS_LOCAL_AGGREGATION=DTrue
export ITERATION=100
export COMP_THRESHOLD=1
export COMPRESSION_RATE=0.01


export EPOCH=1
export BATCH_SIZE=32
export MODEL=mnist
export BERT=bert_24_1024_16
# BERT-BASE: bert_12_768_12 12-layer, 768-hidden, 12-heads
# BERT-LARGE: bert_24_1024_16 24-layer, 1024-hidden, 16-heads

IP_LIST=(xxx.xxx.xxx.xxx)

MASTER=${IP_LIST[0]}
export PORT=xxx
PROC=${MASTER}:${GPUS_PER_NODE}
for(( i=1;i<${NODES};i++))
do
    PROC="${PROC},${IP_LIST[i]}:${GPUS_PER_NODE}"
done
export MASTER_NODE=${MASTER}
export PROC

export NCCL_NIC=ib0
export NCCL_ALGO=Ring
export DEVICE_NUM=$(($GPUS_PER_NODE*$NODES))

export HOROVOD_CYCLE_TIME=2 
export HOROVOD_PERFORM_CYCLE=1 
export HOROVOD_CACHE_CAPACITY=0
export EXPANSION_FACTOR=1
export MXNET_CUDNN_AUTOTUNE_DEFAULT=0
export MXNET_NUM_OF_PARTICIPANTS=${DEVICE_NUM}
export NCCL_DEBUG=INFO
export NCCL_SOCKET_IFNAME=${NCCL_NIC} 

# Forbid Bulk(fusion)
# export MXNET_EXEC_BULK_EXEC_INFERENCE=0
# export MXNET_EXEC_BULK_EXEC_TRAIN=0


export COMP_ALG=dgc
horovodrun --mpi -np ${DEVICE_NUM} --verbose -p ${PORT}\
    -H ${PROC} \
    --network-interface ${NCCL_NIC} \
    bash py_run.sh
