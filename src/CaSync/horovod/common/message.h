// Copyright 2016 The TensorFlow Authors. All Rights Reserved.
// Modifications copyright (C) 2019 Uber Technologies, Inc.
// Modifications copyright Microsoft
// Modifications copyright (C) 2020, NVIDIA CORPORATION. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================

#ifndef HOROVOD_MESSAGE_H
#define HOROVOD_MESSAGE_H

#include <string>
#include <vector>
#include <iostream>

namespace horovod {
namespace common {

enum DataType {
  HOROVOD_UINT8 = 0,
  HOROVOD_INT8 = 1,
  HOROVOD_UINT16 = 2,
  HOROVOD_INT16 = 3,
  HOROVOD_INT32 = 4,
  HOROVOD_INT64 = 5,
  HOROVOD_FLOAT16 = 6,
  HOROVOD_FLOAT32 = 7,
  HOROVOD_FLOAT64 = 8,
  HOROVOD_BOOL = 9,
};

const std::string& DataType_Name(DataType value);

std::size_t DataType_Size(DataType value);

// A Request is a message sent from a rank greater than zero to the
// coordinator (rank zero), informing the coordinator of an operation that
// the rank wants to do and the tensor that it wants to apply the operation to.
class Request {
public:
  enum RequestType {
    ALLREDUCE = 0, ALLGATHER = 1, BROADCAST = 2, JOIN = 3, ADASUM = 4, ALLTOALL = 5, HIPRESSBROADCAST = 6, HIPRESSGATHER = 7, HIPRESSREDUCE = 8, HIPRESSGRADALLTOALL = 9, HIPRESSGRADALLTOALLBCAST = 10
  };


  static const std::string& RequestType_Name(RequestType value);

  // The request rank is necessary to create a consistent ordering of results,
  // for example in the allgather where the order of outputs should be sorted
  // by rank.
  int32_t request_rank() const;

  void set_request_rank(int32_t value);

  RequestType request_type() const;

  void set_request_type(RequestType value);

  DataType tensor_type() const;

  void set_tensor_type(DataType value);

  const std::string& tensor_name() const;

  void set_tensor_name(const std::string& value);

  int32_t root_rank() const;

  void set_root_rank(int32_t value);

  int32_t device() const;

  void set_device(int32_t value);

  int64_t tensor_size() const {
    if (tensor_shape_.size() == 0) {
      return -1;
    }
    int64_t ts = 1;
    for (auto size : tensor_shape_) {
      ts *= size;
    }
    return ts;
  };

  const std::vector<int64_t>& tensor_shape() const;

  void set_tensor_shape(const std::vector<int64_t>& value);

  void add_tensor_shape(int64_t value);

  double prescale_factor() const;

  double postscale_factor() const;

  void set_prescale_factor(const double prescale_factor);

  void set_postscale_factor(const double postscale_factor);

  static void ParseFromBytes(Request& request, const uint8_t* input);

  static void SerializeToString(const Request& request, std::string& output);


private:
  int32_t request_rank_ = 0;
  RequestType request_type_ = RequestType::ALLREDUCE;
  DataType tensor_type_ = DataType::HOROVOD_UINT8;
  int32_t root_rank_ = 0;
  int32_t device_ = 0;
  std::string tensor_name_;
  std::vector<int64_t> tensor_shape_;
  double prescale_factor_ = 1.0;
  double postscale_factor_ = 1.0;
};

class RequestList {
public:
  const std::vector<Request>& requests() const;

  void set_requests(const std::vector<Request>& value);

  void add_request(const Request& value);

  void emplace_request(Request&& value);

  bool shutdown() const;

  void set_shutdown(bool value);

  static void ParseFromBytes(RequestList& request_list,
                             const uint8_t* input);

  static void SerializeToString(const RequestList& request_list,
                                std::string& output);

private:
  std::vector<Request> requests_;
  bool shutdown_ = false;
};

// A Response is a message sent from the coordinator (rank zero) to a rank
// greater than zero, informing the rank of an operation should be performed
// now. If the operation requested would result in an error (for example, due
// to a type or shape mismatch), then the Response can contain an error and
// an error message instead.
class Response {
public:
  enum ResponseType {
    ALLREDUCE = 0, ALLGATHER = 1, BROADCAST = 2, JOIN = 3, ADASUM = 4, ALLTOALL= 5, HIPRESSBROADCAST = 6, HIPRESSGATHER = 7, HIPRESSALLTOALLV = 8, HIPRESSGRADALLTOALL = 9, HIPRESSGRADALLTOALLBCAST = 10, HIPRESSREDUCE = 11, ERROR = 12
  };

  static const std::string& ResponseType_Name(ResponseType value);

  ResponseType response_type() const;

  void set_response_type(ResponseType value);

  // Empty if the type is DONE or SHUTDOWN.
  const std::vector<std::string>& tensor_names() const;

  DataType tensor_type() const;

  void set_tensor_type(DataType value);

  const std::string tensor_names_string() const;

  void set_tensor_names(const std::vector<std::string>& value);

  void add_tensor_name(const std::string& value);

  void add_tensor_name(std::string&& value);

  // Empty unless response_type is ERROR.
  const std::string& error_message() const;

  void set_error_message(const std::string& value);

  const std::vector<int32_t>& devices() const;

  void set_devices(const std::vector<int32_t>& value);

  void add_device(int32_t value);

  // For ALLREDUCE, tensor_sizes() is the shape of the tensor. Multi element for 1 tensor.
  // For GATHER, BROADCAST and ALLTOALL, tensor_sizes() is the total size of the tensor. 1 element for 1 tensor.
  const std::vector<int64_t>& tensor_sizes() const;

  // For GATHER, BROADCAST fusion
  int64_t tensor_size() const{
    int64_t ts = 0;
    for (auto size : tensor_sizes_) {
      ts += size;
    }
    return ts;
  };

  void set_tensor_sizes(const std::vector<int64_t>& value);

  void add_tensor_size(int64_t value);

  // To fuse multiple allgather responses
  void add_allgather_response(const Response& response);

  void set_broadcast_root_rank();

  // fuse multiple gather and broadcast response into one alltoall response
  void add_alltoall_response(const Response& response, bool broadcast);

  double prescale_factor() const;

  double postscale_factor() const;

  void set_prescale_factor(const double prescale_factor);

  void set_postscale_factor(const double postscale_factor);

  static void ParseFromBytes(Response& response, const uint8_t* input);

  static void SerializeToString(const Response& response,
                                std::string& output);

  // clear the response
  void clear();

  // set response with another response
  void set_response(const Response& response);

  void add_root_rank(int32_t value);
  int32_t root_rank(int32_t index) const;
  const std::vector<int32_t>& root_ranks() const;
  void set_root_rank(int32_t index, int32_t value) {
    root_ranks_[index] = value;
  };
  void set_root_ranks(const std::vector<int32_t>& value);

  // set and add etries variablel
  void add_entry_count(int32_t value);
  const std::vector<int32_t>& entries_counts() const;
  void set_entries_counts(const std::vector<int32_t>& value);


private:
  ResponseType response_type_ = ResponseType::ALLREDUCE;
  std::vector<std::string> tensor_names_;
  DataType tensor_type_ = DataType::HOROVOD_UINT8;
  std::string error_message_;
  std::vector<int32_t> devices_;
  std::vector<int64_t> tensor_sizes_;
  double prescale_factor_ = 1.0;
  double postscale_factor_ = 1.0;
  std::vector<int32_t> entries_counts_;
  std::vector<int32_t> root_ranks_; // roots that will do gather concurrently
};

class ResponseList {
public:
  const std::vector<Response>& responses() const;

  void set_responses(const std::vector<Response>& value);

  void add_response(const Response& value);

  void add_response(Response&& value);

  void emplace_response(Response&& value);

  bool shutdown() const;

  void set_shutdown(bool value);

  std::vector<Response> get_mpi_responses();

  int mpi_index() const;

  void set_mpi_index(int value);

  static void ParseFromBytes(ResponseList& response_list,
                             const uint8_t* input);

  static void SerializeToString(const ResponseList& response_list,
                                std::string& output);

private:
  std::vector<Response> responses_;
  bool shutdown_ = false;
  int mpi_index_ = 0;
};

class AlltoallvEntry {
public:
  // Empty if the type is DONE or SHUTDOWN.
  const std::vector<std::string>& tensor_names() const;

  const std::string tensor_names_string() const;

  void set_tensor_names(const std::vector<std::string>& value);

  void add_tensor_name(const std::string& value);

  void add_tensor_name(std::string&& value);

  void ParseFromBytes(AlltoallvEntry& entry, const uint8_t* input);

  void SerializeToString(const AlltoallvEntry& entry,
                                std::string& output);

private:
  std::vector<std::string> tensor_names_;
};

class ResponseAlltoallv {
public:
  ResponseAlltoallv(int nranks) : nranks_(nranks) {};
  
  void init() {
    entries_.reserve(nranks_ * nranks_);
    for (int i = 0; i < nranks_ * nranks_; i++) {
      AlltoallvEntry e;
      entries_.emplace_back(std::move(e));
    }
  };

  std::vector<AlltoallvEntry> send_entries(int my_rank) const {
    std::vector<AlltoallvEntry> send_entries = {entries_.begin() + my_rank * nranks_, entries_.begin() + (my_rank + 1) * nranks_};
    return send_entries;
  };

  std::vector<AlltoallvEntry> recv_entries(int my_rank) const {
    std::vector<AlltoallvEntry> recv_entries;
    recv_entries.reserve(nranks_);
    for (int i = 0; i < nranks_; i++) {
      if (entries_[i * nranks_ + my_rank].tensor_names_string() == "") {
        AlltoallvEntry entry;
        recv_entries.push_back(entry);
      } else {
        recv_entries.push_back(entries_[i * nranks_ + my_rank]);
      }
    }
    return recv_entries;
  };  

  const std::vector<AlltoallvEntry>& entries() const;

  void set_response_alltoallv(std::vector<AlltoallvEntry> value);

  void add_entry(AlltoallvEntry&& value);

  AlltoallvEntry& get_entry(int index) {
    return entries_[index];
  }

  AlltoallvEntry& get_entry(int i, int j) {
    return entries_[i * nranks_ + j];
  }

  int nranks() const;

  void set_nranks(int value);

  const std::vector<std::string>& tensor_names() const;

  const std::string tensor_names_string() const;

  void set_tensor_names(const std::vector<std::string>& value);

  void add_tensor_name(const std::string& value);

  void add_tensor_name(std::string&& value);

  void emplace_tensor_name(std::string& value);

  bool shutdown() const;

  void set_shutdown(bool value);

  void clear();

  static void ParseFromBytes(ResponseAlltoallv& response_alltoallv,
                             const uint8_t* input);

  static void SerializeToString(const ResponseAlltoallv& response_alltoallv,
                                std::string& output);

private:
  std::vector<AlltoallvEntry> entries_;
  std::vector<std::string> tensor_names_;
  int nranks_;
  bool shutdown_ = false;
};

} // namespace common
} // namespace horovod

#endif // HOROVOD_MESSAGE_H
