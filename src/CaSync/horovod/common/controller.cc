// Copyright 2019 Uber Technologies, Inc. All Rights Reserved.
// Modifications copyright Microsoft
// Modifications copyright (C) 2020, NVIDIA CORPORATION. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================

#include "controller.h"

#include <atomic>
#include <map>
#include <queue>
#include <set>
#include <unordered_set>

#include "global_state.h"
#include "logging.h"
#include "operations.h"

namespace horovod {
namespace common {


void Controller::SetCoordinator() {
  switch (communicator_) {
    case Communicator::GLOBAL:
      is_coordinator_ = (rank_ == 0);
      break;
    case Communicator::CROSS:
      is_coordinator_ = ((local_rank_ == local_root) && (cross_rank_ == cross_root));
      with_cross_ = (local_rank_ == local_root);
      break;
    case Communicator::LOCAL:
      is_coordinator_ = (local_rank_ == local_root);
      break;
    default:
      throw std::logic_error("Communicator type " + CommunicatorName(communicator_) + " is not supported");
  }
}

void Controller::SynchronizeParameters() {
  ParameterManager::Params param;
  if (is_coordinator_) {
    param = parameter_manager_.GetParams();
  }

  void* buffer = (void*)(&param);
  size_t param_size = sizeof(param);
  Bcast(buffer, param_size, 0, communicator_);

  if (!is_coordinator_) {
    parameter_manager_.SetParams(param);
  }
  parameter_manager_.Reset();
}

Controller::Controller(ResponseCache& response_cache, TensorQueue& tensor_queue,
                       Timeline& timeline, ParameterManager& parameter_manager)
    : stall_inspector_(response_cache), tensor_queue_(tensor_queue),
      timeline_(timeline), response_cache_(response_cache),
      parameter_manager_(parameter_manager) {}

Controller::~Controller() {
  delete [] gather_responses_;
  delete [] broadcast_responses_;
}

void Controller::Initialize() {
  response_cache_.clear();

  // Initialize concrete implementations.
  DoInitialization();
  gather_responses_ = new std::vector<Response> [GetCommSize()];
  broadcast_responses_ = new std::vector<Response> [GetCommSize()];
}

ResponseList Controller::ComputeResponseList(std::atomic_bool& shut_down,
                                             HorovodGlobalState& state,
                                             ResponseAlltoallv& response_alltoallv) {
  // Update cache capacity if autotuning is active.
  if (parameter_manager_.IsAutoTuning()) {
    response_cache_.set_capacity((int)parameter_manager_.CacheEnabled() *
                                 cache_capacity_);
  }

  // Copy the data structures out from parameters.
  // However, don't keep the lock for the rest of the loop, so that
  // enqueued stream callbacks can continue.

  CacheCoordinator cache_coordinator(response_cache_.num_active_bits());

  // message queue used only in this cycle
  std::deque<Request> message_queue_tmp;
  tensor_queue_.PopMessagesFromQueue(message_queue_tmp);
  for (auto& message : message_queue_tmp) {
    if (message.request_type() == Request::JOIN) {
      state.joined = true;
      cache_coordinator.set_uncached_in_queue(true);
      continue;
    }

    // Keep track of cache hits
    if (response_cache_.capacity() > 0) {
      auto cache_ = response_cache_.cached(message);
      if (cache_ == ResponseCache::CacheState::HIT) {
        uint32_t cache_bit = response_cache_.peek_cache_bit(message);
        cache_coordinator.record_hit(cache_bit);

        // Record initial time cached tensor is encountered in queue.
        stall_inspector_.RecordCachedTensorStart(message.tensor_name());

      } else {
        if (cache_ == ResponseCache::CacheState::INVALID) {
          uint32_t cache_bit = response_cache_.peek_cache_bit(message);
          cache_coordinator.record_invalid_bit(cache_bit);
        }
        cache_coordinator.set_uncached_in_queue(true);

        // Remove timing entry if uncached or marked invalid.
        stall_inspector_.RemoveCachedTensor(message.tensor_name());
      }
    }
  }

  if (state.joined && response_cache_.capacity() > 0) {
    for (uint32_t bit : response_cache_.list_all_bits()) {
      cache_coordinator.record_hit(bit);
    }
  }

  // Flag indicating that the background thread should shut down.
  bool should_shut_down = shut_down;

  // Check for stalled tensors.
  if (stall_inspector_.ShouldPerformCheck()) {
    if (is_coordinator_) {
      should_shut_down |= stall_inspector_.CheckForStalledTensors(GetCommSize());
    }

    if (response_cache_.capacity() > 0) {
      stall_inspector_.InvalidateStalledCachedTensors(cache_coordinator);
    }
    stall_inspector_.UpdateCheckTime();
  }

  cache_coordinator.set_should_shut_down(should_shut_down);

  if (response_cache_.capacity() > 0) {
    // Obtain common cache hits and cache invalidations across workers. Also,
    // determine if any worker has uncached messages in queue or requests
    // a shutdown. This function removes any invalid cache entries, if they
    // exist.
    CoordinateCacheAndState(cache_coordinator);
    // Remove uncommon cached tensors from queue and replace to state
    // queue for next cycle. Skip adding common cached tensors to
    // queue as they are handled separately.
    std::deque<Request> messages_to_replace;
    size_t num_messages = message_queue_tmp.size();
    for (size_t i = 0; i < num_messages; ++i) {
      auto& message = message_queue_tmp.front();
      if (response_cache_.cached(message) == ResponseCache::CacheState::HIT) {
        uint32_t cache_bit = response_cache_.peek_cache_bit(message);
        if (cache_coordinator.cache_hits().find(cache_bit) ==
            cache_coordinator.cache_hits().end()) {
          // Try to process again in next cycle.
          messages_to_replace.push_back(std::move(message));
        } else {
          // Remove timing entry for messages being handled this cycle.
          stall_inspector_.RemoveCachedTensor(message.tensor_name());
        }
      } else {
        // Remove timing entry for messages being handled this cycle.
        stall_inspector_.RemoveCachedTensor(message.tensor_name());
        message_queue_tmp.push_back(std::move(message));
      }
      message_queue_tmp.pop_front();
    }
    tensor_queue_.PushMessagesToQueue(messages_to_replace);
  }

  if (!message_queue_tmp.empty()) {
    LOG(TRACE, rank_) << "Sent " << message_queue_tmp.size()
                      << " messages to coordinator.";
  }

  ResponseList response_list;
  response_list.set_shutdown(cache_coordinator.should_shut_down());

  bool need_communication = true;
  if (response_cache_.capacity() > 0 &&
      !cache_coordinator.uncached_in_queue()) {
    // if cache is enabled and no uncached new message coming in, no need for
    // additional communications
    need_communication = false;

    // If no messages to send, we can simply return an empty response list;
    if (cache_coordinator.cache_hits().empty()) {
      return response_list;
    }
    // otherwise we need to add cached messages to response list.
  }

  if (!need_communication) {
    // If all messages in queue have responses in cache, use fast path with
    // no additional coordination.

    std::deque<Response> responses;
    // Convert cache hits to responses. Populate so that least
    // recently used responses get priority. All workers call the code
    // here so we use the get method here to consistently update the cache
    // order.
    for (auto bit : cache_coordinator.cache_hits()) {
      responses.push_back(response_cache_.get_response(bit));
    }

    // Fuse responses as normal.
    response_list = FuseResponses(responses, response_alltoallv, state.using_gcl);
    response_list.set_shutdown(cache_coordinator.should_shut_down());
  } else {
    // There are uncached messages coming in, need communication to figure out
    // whether those are ready to be reduced.

    // Collect all tensors that are ready to be reduced. Record them in the
    // tensor count table (rank zero) or send them to rank zero to be
    // recorded (everyone else).
    std::vector<std::string> ready_to_reduce;

    if (is_coordinator_) {
      
      while (!message_queue_tmp.empty()) {
        // Pop the first available message
        Request message = message_queue_tmp.front();
        message_queue_tmp.pop_front();

        if (message.request_type() == Request::JOIN) {
          state.joined_size++;
          continue;
        }
        LOG(TRACE) << "Adding messages from rank "<< CommunicatorName(communicator_) << " 0 :" << message.tensor_name();
        bool reduce = IncrementTensorCount(message, state.joined_size);
        stall_inspector_.RecordUncachedTensorStart(
            message.tensor_name(), message.request_rank(), GetCommSize());
        
        if (reduce) {
          ready_to_reduce.push_back(message.tensor_name()); 
        }
      }

      // Receive ready tensors from other ranks
      std::vector<RequestList> ready_list;
      RecvReadyTensors(ready_to_reduce, ready_list);

      // Process messages.
      for (int i = 1; i < GetCommSize(); ++i) {
        auto received_message_list = ready_list[i];
        for (auto& received_message : received_message_list.requests()) {
          auto& received_name = received_message.tensor_name();

          if (received_message.request_type() == Request::JOIN) {
            state.joined_size++;
            continue;
          }
          LOG(TRACE) << "Adding messages from "<< CommunicatorName(communicator_) << " rank " << i << ": "<< received_name;
          bool reduce = IncrementTensorCount(received_message, state.joined_size);
          stall_inspector_.RecordUncachedTensorStart(
              received_message.tensor_name(), received_message.request_rank(),
              GetCommSize());
          if (reduce) {
            ready_to_reduce.push_back(received_name);
          }
        }
        if (received_message_list.shutdown()) {
          // Received SHUTDOWN request from one of the workers.
          should_shut_down = true;
        }
      }

      // Check if tensors from previous ticks are ready to reduce after Joins.
      if (state.joined_size > 0) {
        for (auto& table_iter : message_table_) {
          int count = (int)table_iter.second.size();
          if (count == (GetCommSize() - state.joined_size) &&
              std::find(ready_to_reduce.begin(), ready_to_reduce.end(),
                        table_iter.first) == ready_to_reduce.end()) {
            state.timeline.NegotiateEnd(table_iter.first);
            ready_to_reduce.push_back(table_iter.first);
          }
        }
      }

      // At this point, rank zero should have a fully updated tensor count
      // table and should know all the tensors that need to be reduced or
      // gathered, and everyone else should have sent all their information
      // to rank zero. We can now do reductions and gathers; rank zero will
      // choose which ones and in what order, and will notify the other ranks
      // before doing each reduction.
      std::deque<Response> responses;

      if (response_cache_.capacity() > 0) {
        // Prepopulate response list with cached responses. Populate so that
        // least recently used responses get priority. Since only the
        // coordinator rank calls this code, use peek instead of get here to
        // preserve cache order across workers.
        // No need to do this when all ranks did Join.
        if (state.joined_size < GetCommSize()) {
          for (auto bit : cache_coordinator.cache_hits()) {
            responses.push_back(response_cache_.peek_response(bit));
          }
        }
      }

      for (auto& tensor_name : ready_to_reduce) {
        Response response = ConstructResponse(tensor_name, state.joined_size);
        responses.push_back(std::move(response));
        LOG(DEBUG) << "Constructed "<< CommunicatorName(communicator_) << " response for " << tensor_name;
      }
      if (state.joined_size == GetCommSize()) {
        // All ranks did Join(). Send the response, reset joined size.
        Response join_response;
        join_response.set_response_type(Response::JOIN);
        join_response.add_tensor_name(JOIN_TENSOR_NAME);
        responses.push_back(std::move(join_response));
        state.joined_size = 0;
      }
      response_list = FuseResponses(responses, response_alltoallv, state.using_gcl);
      response_list.set_shutdown(should_shut_down);
      // Broadcast final results to other ranks.
      if (state.using_gcl) {
        SendFinalTensors(response_list, response_alltoallv);
      }
      SendFinalTensors(response_list);

    } else {
      RequestList message_list;
      message_list.set_shutdown(should_shut_down);
      while (!message_queue_tmp.empty()) {
        message_list.add_request(message_queue_tmp.front());
        message_queue_tmp.pop_front();
      }

      // Send ready tensors to rank zero
      SendReadyTensors(message_list);

      // Receive final tensors to be processed from rank zero
      if (state.using_gcl) {
        RecvFinalTensors(response_list, response_alltoallv);
      }
      RecvFinalTensors(response_list);
    }
  }

  if (!response_list.responses().empty()) {
    std::string tensors_ready;
    for (const auto& r : response_list.responses()) {
      tensors_ready += r.tensor_names_string() + "; ";
    }
    LOG(TRACE) << "Sending ready "<< CommunicatorName(communicator_) << " responses as " << tensors_ready;
  }
  if (state.using_gcl && response_alltoallv.tensor_names().size() > 0) {
    LOG(TRACE) << "GCL alltoallv fused tensors:" << response_alltoallv.tensor_names_string();
  }

  // If need_communication is false, meaning no uncached message coming in,
  // thus no need to update cache.
  if (need_communication && response_cache_.capacity() > 0) {
    // All workers add supported responses to cache. This updates the cache
    // order consistently across workers.
    for (auto& response : response_list.responses()) {
      if ((response.response_type() == Response::ResponseType::ALLREDUCE ||
           response.response_type() == Response::ResponseType::ADASUM ||
           response.response_type() == Response::ResponseType::ALLTOALL) &&
          (int)response.devices().size() == GetCommSize()) {
        response_cache_.put(response, tensor_queue_, state.joined);
      }
    }
  }

  // Reassign cache bits based on current cache order.
  response_cache_.update_cache_bits();

  return response_list;
}

int64_t Controller::TensorFusionThresholdBytes() {
  int64_t proposed_fusion_threshold =
      parameter_manager_.TensorFusionThresholdBytes();

  // If the cluster is homogeneous,
  // adjust buffer size to make sure it is divisible by local_size to improve
  // performance for operations that perform local reductions by default such as Adasum.
  if (is_homogeneous_) {
    // Assume the worst-case data type float64, since if it is divisible with
    // float64, it will be divisible for other types too.

    // Ensuring that fusion buffer can hold a number of elements divisible by
    // FUSION_BUFFER_ATOMIC_UNIT for performance
    int double_size = GetTypeSize(HOROVOD_FLOAT64);
    int64_t div = local_size_ * double_size * FUSION_BUFFER_ATOMIC_UNIT;
    return ((proposed_fusion_threshold + div - 1) / div) * div;
  }
  return proposed_fusion_threshold;
}

Response Controller::ConstructResponse(std::string& name, int joined_size) {
  bool error = false;
  auto it = message_table_.find(name);
  assert(it != message_table_.end());

  std::vector<Request>& requests = it->second;
  assert(!requests.empty());

  std::ostringstream error_message_stream;

  // Check that all data types of tensors being processed
  // are identical.
  auto data_type = requests[0].tensor_type();
  for (unsigned int i = 1; i < requests.size(); ++i) {
    auto request_type = requests[i].tensor_type();
    if (data_type != request_type) {
      error = true;
      error_message_stream << "Mismatched data types: One rank had type "
                           << DataType_Name(data_type)
                           << ", but another rank had type "
                           << DataType_Name(request_type) << ".";
      break;
    }
  }

  // Check that all requested operations are the same
  auto message_type = requests[0].request_type();
  for (unsigned int i = 1; i < requests.size(); ++i) {
    if (error) {
      break;
    }

    auto request_type = requests[i].request_type();
    if (message_type != request_type) {
      error = true;
      error_message_stream << "Mismatched operations: One rank did an "
                           << Request::RequestType_Name(message_type)
                           << ", but another rank did an "
                           << Request::RequestType_Name(request_type) << ".";
      break;
    }
  }

  // If we are doing an allreduce or broadcast, check that all tensor shapes are
  // identical.
  int32_t cbroadcast_root_idx = 0;
  if (message_type == Request::HIPRESSBROADCAST){
    int32_t root_id = requests[0].root_rank();
    for (unsigned int i=0; i< requests.size(); ++i){
      if (root_id == requests[i].request_rank()){
        cbroadcast_root_idx = i;
        break;
      }
    }
  }
  if (message_type == Request::ALLREDUCE ||
      message_type == Request::ADASUM ||
      message_type == Request::BROADCAST) {
    TensorShape tensor_shape;
    for (auto dim : requests[0].tensor_shape()) {
      tensor_shape.AddDim(dim);
    }
    for (unsigned int i = 1; i < requests.size(); ++i) {
      if (error) {
        break;
      }

      TensorShape request_shape;
      for (auto dim : requests[i].tensor_shape()) {
        request_shape.AddDim(dim);
      }
      if (tensor_shape != request_shape) {
        error = true;
        error_message_stream
            << "Mismatched " << Request::RequestType_Name(message_type)
            << " tensor shapes: One rank sent a tensor of shape "
            << tensor_shape.DebugString()
            << ", but another rank sent a tensor of shape "
            << request_shape.DebugString() << ".";
        break;
      }
    }
  }

  // If we are doing an allreduce, check that prescaling and postscaling factors
  // are identical across ranks.
  double prescale_factor;
  double postscale_factor;
  if (message_type == Request::ALLREDUCE ||
      message_type == Request::ADASUM) {
    prescale_factor = requests[0].prescale_factor();
    postscale_factor = requests[0].postscale_factor();

    for (unsigned int i = 1; i < requests.size(); ++i) {
      if (error) {
        break;
      }
      double request_prescale_factor = requests[i].prescale_factor();
      double request_postscale_factor = requests[i].postscale_factor();

      if (prescale_factor != request_prescale_factor ||
          postscale_factor != request_postscale_factor) {
        error = true;
        error_message_stream
            << "Mismatched prescale and/or postscale factors: "
            << "One rank sent factors (" << prescale_factor
            << ", " << postscale_factor << "), but another rank "
            << "sent factors (" << request_prescale_factor
            << ", " << request_postscale_factor << ").";
        break;
      }
    }
  }

  std::vector<int64_t> tensor_sizes;
  if (message_type == Request::ALLGATHER ||
      message_type == Request::ALLTOALL ||
      message_type == Request::HIPRESSGATHER) {
    if (joined_size > 0) {
      error = true;
      if (message_type == Request::ALLGATHER) {
        error_message_stream << "Allgather is not supported with Join at this time. "
                             << "Specify sparse_to_dense=True if using DistributedOptimizer";
      } else if (message_type == Request::ALLTOALL) {
        error_message_stream << "Alltoall is not supported with Join at this time.";
      }
    }

    // If we are doing an allgather/alltoall, make sure all but the first dimension are
    // the same. The first dimension may be different and the output tensor is
    // the sum of the first dimension. Collect the sizes by rank for allgather only.
    tensor_sizes.resize(requests.size());
    TensorShape tensor_shape;
    for (auto dim : requests[0].tensor_shape()) {
      tensor_shape.AddDim(dim);
    }

    if (tensor_shape.dims() == 0) {
      error = true;
      error_message_stream << "Rank zero tried to "
                           << Request::RequestType_Name(message_type)
                           << " a rank-zero tensor.";
    } else {
      tensor_sizes[requests[0].request_rank()] = tensor_shape.dim_size(0);
    }

    for (unsigned int i = 1; i < requests.size(); ++i) {
      if (error) {
        break;
      }

      TensorShape request_shape;
      for (auto dim : requests[i].tensor_shape()) {
        request_shape.AddDim(dim);
      }
      if (tensor_shape.dims() != request_shape.dims()) {
        error = true;
        error_message_stream
            << "Mismatched " << Request::RequestType_Name(message_type)
            << " tensor shapes: One rank sent a tensor of rank "
            << tensor_shape.dims()
            << ", but another rank sent a tensor of rank "
            << request_shape.dims() << ".";
        break;
      }

      bool dim_mismatch = false;
      for (int dim = 1; dim < tensor_shape.dims(); ++dim) {
        if (tensor_shape.dim_size(dim) != request_shape.dim_size(dim)) {
          error = true;
          error_message_stream
              << "Mismatched " << Request::RequestType_Name(message_type)
              << " tensor shapes: One rank sent a tensor with dimension " << dim
              << " equal to " << tensor_shape.dim_size(dim)
              << ", but another rank sent a tensor with dimension " << dim
              << " equal to " << request_shape.dim_size(dim) << ".";
          dim_mismatch = true;
          break;
        }
      }
      if (dim_mismatch) {
        break;
      }

      // Collect first dimension sizes for allgather to use for fusion and allgather op.
      if (message_type == Request::ALLGATHER) {
        tensor_sizes[requests[i].request_rank()] = request_shape.dim_size(0);
      }
    }
  }

  if (message_type == Request::ALLREDUCE || message_type == Request::ADASUM ||
  message_type == Request::BROADCAST) {
    TensorShape tensor_shape;
    for (auto dim : requests[0].tensor_shape()) {
      tensor_shape.AddDim(dim);
    }
    tensor_sizes.push_back(tensor_shape.num_elements());
  }

  if (message_type == Request::BROADCAST ||
      message_type == Request::HIPRESSBROADCAST ||
      message_type == Request::HIPRESSGATHER) {
    if (joined_size > 0) {
      error = true;
      error_message_stream << "Broadcast is not supported with Join at this time.";
    }

    // If we are doing a broadcast, check that all root ranks are identical.
    int first_root_rank = requests[0].root_rank();
    for (unsigned int i = 1; i < requests.size(); ++i) {
      if (error) {
        break;
      }

      int this_root_rank = requests[i].root_rank();
      if (first_root_rank != this_root_rank) {
        error = true;
        error_message_stream
            << "Mismatched " << Request::RequestType_Name(message_type)
            << " root ranks: One rank specified root rank " << first_root_rank
            << ", but another rank specified root rank " << this_root_rank
            << ".";
        break;
      }
    }
  }

  bool first_device_is_cpu = requests[0].device() == CPU_DEVICE_ID;
  for (unsigned int i = 1; i < requests.size(); ++i) {
    if (error) {
      break;
    }

    bool this_device_is_cpu = requests[i].device() == CPU_DEVICE_ID;
    if (first_device_is_cpu != this_device_is_cpu) {
      error = true;
      error_message_stream
          << "Mismatched " << Request::RequestType_Name(message_type)
          << " CPU/GPU device selection: One rank specified device "
          << (first_device_is_cpu ? "CPU" : "GPU")
          << ", but another rank specified device "
          << (this_device_is_cpu ? "CPU" : "GPU") << ".";
      break;
    }
  }
  std::vector<int32_t> devices(requests.size());
  for (auto& request : requests) {
    LOG(DEBUG) << "request_name " <<  request.tensor_name() << " request_rank " << request.request_rank() << " device "
               << request.device();
    devices[request.request_rank()] = request.device();
  }

  Response response;
  response.add_tensor_name(name);
  response.set_devices(devices);
  response.add_root_rank(requests[0].root_rank());
  if (error) {
    std::string error_message = error_message_stream.str();
    response.set_response_type(Response::ERROR);
    response.set_error_message(error_message);
  } else {
    switch (message_type) {
      case Request::ALLGATHER:
      {
        response.set_response_type(Response::ALLGATHER);
        for (auto dim : tensor_sizes) {
          response.add_tensor_size(dim);
        }
        break;
      }
      case Request::ALLREDUCE:
      {
        response.set_response_type(Response::ALLREDUCE);
        for (auto dim : tensor_sizes) {
          response.add_tensor_size(dim);
        }
        response.set_tensor_type(data_type);
        response.set_prescale_factor(prescale_factor);
        response.set_postscale_factor(postscale_factor);
        break;
      }
      case Request::BROADCAST:
      {
        response.add_tensor_size(tensor_sizes[0]);
        response.set_response_type(Response::BROADCAST);
        break;
      }
      case Request::ALLTOALL:
      { 
        response.set_response_type(Response::ALLTOALL);
        break;
      }
      case Request::HIPRESSREDUCE:
      {
        response.set_response_type(Response::HIPRESSREDUCE);
        response.add_tensor_size(requests[0].tensor_size());
        break;
      }
      case Request::HIPRESSBROADCAST:
      {
        response.set_response_type(Response::HIPRESSBROADCAST);
        response.add_tensor_size(requests[cbroadcast_root_idx].tensor_size());
        break;
      }
      case Request::HIPRESSGATHER:
      {
        response.set_response_type(Response::HIPRESSGATHER);
        response.add_tensor_size(requests[0].tensor_size());
        break;
      }
      case Request::HIPRESSGRADALLTOALL:
      {
        LOG(DEBUG) << "HIPRESSGRADALLTOALL response tensor_size " << response.tensor_size();
        response.set_response_type(Response::HIPRESSGRADALLTOALL);
        break;
      }
      case Request::HIPRESSGRADALLTOALLBCAST:
      {
        std::vector<int> requeset_tensor_size(requests.size());
        for (auto& request : requests) {
          requeset_tensor_size[request.request_rank()] = request.tensor_size();
        }
        int max_tensor_size = *std::max_element(requeset_tensor_size.begin(), requeset_tensor_size.end());
        response.add_tensor_size(max_tensor_size);
        LOG(DEBUG) << "HIPRESSGRADALLTOALLBCAST response tensor_size " << response.tensor_size();
        response.set_response_type(Response::HIPRESSGRADALLTOALLBCAST);
        break;
      }
      case Request::ADASUM:
      {
        response.set_response_type(Response::ADASUM);
        for (auto dim : tensor_sizes) {
          response.add_tensor_size(dim);
        }
        response.set_tensor_type(data_type);
        response.set_prescale_factor(prescale_factor);
        response.set_postscale_factor(postscale_factor);
        break;
      }
      default:
      {
        throw std::runtime_error("Invalid request type: " + std::to_string(message_type));
        break;
      }
    }
  }


  // Clear all queued up requests for this name. They are now taken care of
  // by the constructed response.
  message_table_.erase(it);
  stall_inspector_.RemoveUncachedTensor(name);

  return response;
}

void Controller::CoordinateCacheAndState(CacheCoordinator& cache_coordinator) {
  // Sync cache and state information across workers.
  cache_coordinator.sync(shared_from_this(), timeline_enabled_);

  // If invalid cache entries exist, erase associated entries.
  if (!cache_coordinator.invalid_bits().empty()) {
    for (auto bit : cache_coordinator.invalid_bits()) {
      response_cache_.erase_response(bit);
    }
  }

  if (timeline_enabled_) {
    // Start/continue negotiation phase on timeline bit entries.
    for (auto bit : cache_coordinator.timeline_bits()) {
      auto& response = response_cache_.peek_response(bit);
      timeline_.NegotiateStart(response.tensor_names()[0],
                               (Request::RequestType)response.response_type());
    }

    // End negotiation phase for synced cache hit set entries.
    for (auto bit : cache_coordinator.cache_hits()) {
      auto& response = response_cache_.peek_response(bit);
      timeline_.NegotiateEnd(response.tensor_names()[0]);
    }
  }
}

std::vector<Response>& Controller::gather_vector(int idx) {
  return gather_responses_[idx];
}

std::vector<Response>& Controller::broadcast_vector(int idx) {
  return broadcast_responses_[idx];
}


ResponseList Controller::FuseResponses(std::deque<Response>& responses, ResponseAlltoallv& response_alltoallv, bool using_gcl) {
  ResponseList response_list;

  Response ar_response, ag_response, aa_response, aab_response; // allreduce, allgather, hipressgradalltoall, hipressgradalltoallbcast
  DataType ar_dtype, ag_dtype, aa_dtype, aab_dtype;
  int64_t ar_tensor_size=0, ag_tensor_size=0, aa_tensor_size=0, aab_tensor_size=0;

  int gather_count = 0;
  int broadcast_count = 0;

  while (!responses.empty()) {
    auto response = responses.front();
    assert(response.tensor_names().size() == 1);
    responses.pop_front();
    auto& entry = tensor_queue_.GetTensorEntry(response.tensor_names()[0]);
    LOG(DEBUG) << "Fusion: response " << response.tensor_names()[0] << " type " << response.response_type();
    switch (response.response_type()) {
      case Response::ResponseType::ALLREDUCE:
      {
        if (ar_response.tensor_names().size() == 0) {
          ar_response.set_response(response);
          ar_dtype = entry.tensor->dtype();
          ar_tensor_size = entry.tensor->size();
        } else {
          if (response.response_type() == ar_response.response_type() &&
              response.devices() == ar_response.devices() &&
              entry.tensor->dtype() == ar_dtype &&
              ar_tensor_size + entry.tensor->size() <= TensorFusionThresholdBytes()) {
            ar_tensor_size += entry.tensor->size();
            ar_response.add_tensor_name(response.tensor_names()[0]);
          } else {
            response_list.add_response(ar_response);
            ar_response.clear();
            ar_response.set_response(response);
            ar_dtype = entry.tensor->dtype();
            ar_tensor_size = entry.tensor->size();
          }
        }
        break;
      }
      case Response::ResponseType::ALLGATHER:
      {
        if (ag_response.tensor_names().size() == 0) {
          ag_response.set_response(response);
          ag_dtype = entry.tensor->dtype();
          ag_tensor_size = TotalByteSizeOfAllgatherOutput(response.tensor_sizes(), entry);
        } else {
          int64_t new_allgather_byte_size = TotalByteSizeOfAllgatherOutput(response.tensor_sizes(), entry);
          if (response.response_type() == ag_response.response_type() &&
              response.devices() == ag_response.devices() &&
              entry.tensor->dtype() == ag_dtype &&
              ag_tensor_size + new_allgather_byte_size <= TensorFusionThresholdBytes()) {
            ag_tensor_size += new_allgather_byte_size;
            ag_response.add_allgather_response(response);
          } else {
            response_list.add_response(ag_response);
            ag_response.clear();
            ag_response.set_response(response);
            ag_dtype = entry.tensor->dtype();
            ag_tensor_size = TotalByteSizeOfAllgatherOutput(response.tensor_sizes(), entry);
          }
        }
        break;
      }
      case Response::ResponseType::HIPRESSGATHER:
      {
        LOG(DEBUG) << "new HIPRESSGATHER come name: " << response.tensor_names()[0] << ", root_rank:" << response.root_rank(0) << ", size: " << response.tensor_size();
        if (using_gcl) {
          continue;
        }
        int element_size = GetTypeSize(entry.tensor->dtype());
        auto& gather_response_vec = gather_vector(response.root_rank(0));
        if (gather_response_vec.empty()){
          LOG(TRACE) << "new gather response :" << response.tensor_names()[0];
          gather_response_vec.push_back(std::move(response));
          gather_count++;
        } else {
          auto& last_response = gather_response_vec.back();
          if (last_response.devices() == response.devices() &&
              last_response.tensor_type() == response.tensor_type() &&
              last_response.tensor_size() + response.tensor_size() <= TensorFusionThresholdBytes()/element_size) {
            last_response.add_tensor_name(response.tensor_names()[0]);
            last_response.add_tensor_size(response.tensor_sizes()[0]);
            LOG(TRACE) << "Fusing gather response result" << last_response.tensor_names_string();
          } else {
            LOG(TRACE) << "cannot fusion, new gather response :" << response.tensor_names()[0];
            gather_response_vec.push_back(std::move(response));
            gather_count++;
          }
        }
        break;
      }
      case Response::ResponseType::HIPRESSBROADCAST:
      {
        LOG(DEBUG) << "new HIPRESSBROADCAST come name: " << response.tensor_names()[0] << ", root_rank:" << response.root_rank(0) << ", size: " << response.tensor_size();
        if (communicator_ == Communicator::LOCAL) {
          LOG(DEBUG) << "local communicator, no need to fusion broadcast";
          response_list.add_response(response);
          continue;
        }
        if (using_gcl) {
          continue;
        }
        int element_size = GetTypeSize(entry.tensor->dtype());
        auto& broadcast_response_vec = broadcast_vector(response.root_rank(0));
        if (broadcast_response_vec.empty()){
          LOG(TRACE) << "new broadcast response :" << response.tensor_names()[0];
          broadcast_response_vec.push_back(std::move(response));
          broadcast_count++;
        } else {
          auto& last_response = broadcast_response_vec.back();
          if (last_response.response_type() == response.response_type() &&
              last_response.devices() == response.devices() &&
              last_response.tensor_type() == response.tensor_type() &&
              last_response.tensor_size() + response.tensor_size() <= TensorFusionThresholdBytes()/element_size) {
            last_response.add_tensor_name(response.tensor_names()[0]);
            last_response.add_tensor_size(response.tensor_sizes()[0]);
            LOG(TRACE) << "Fusing broadcast response result" << last_response.tensor_names_string();
          } else {
            LOG(TRACE) << "cannot fusion, new broadcast response :" << response.tensor_names()[0];
            broadcast_response_vec.push_back(response);
            broadcast_count++;
          }
        }
        break;
      }
      case Response::ResponseType::HIPRESSGRADALLTOALL:
      {
        if (aa_response.tensor_names().size() == 0) {
          aa_response.set_response(response);
          aa_dtype = entry.tensor->dtype();
          aa_tensor_size = entry.num_elem;
        } else {
          if (response.response_type() == aa_response.response_type() &&
              response.devices() == aa_response.devices() &&
              entry.tensor->dtype() == aa_dtype &&
              aa_tensor_size + entry.num_elem <= TensorFusionThresholdBytes() ){
            aa_tensor_size += entry.num_elem;
            aa_response.add_tensor_name(response.tensor_names()[0]);
          } else {
            response_list.add_response(aa_response);
            aa_response.clear();
            aa_response.set_response(response);
            aa_dtype = entry.tensor->dtype();
            aa_tensor_size = entry.num_elem;
          }
        }
        break;
      }
      case Response::ResponseType::HIPRESSGRADALLTOALLBCAST:
      {
        LOG(DEBUG) << "new HIPRESSGRADALLTOALLBCAST come name: " << response.tensor_names()[0] << ", size: " << response.tensor_size();
        if (aab_response.tensor_names().size() == 0) {
          aab_response.set_response(response);
          aab_dtype = entry.tensor->dtype();
          aab_tensor_size = response.tensor_size();
        } else {
          int64_t new_tensor_size = response.tensor_size();
          if (response.response_type() == aab_response.response_type() &&
              response.devices() == aab_response.devices() &&
              entry.tensor->dtype() == aab_dtype &&
              aab_tensor_size + new_tensor_size <= TensorFusionThresholdBytes() ){
            aab_tensor_size += new_tensor_size;
            aab_response.add_tensor_name(response.tensor_names()[0]);
            aab_response.add_tensor_size(new_tensor_size);
          } else {
            response_list.add_response(aab_response);
            aab_response.clear();
            aab_response.set_response(response);
            aab_dtype = entry.tensor->dtype();
            aab_tensor_size = response.tensor_size();
          }
        }
        break;
      }
      default:
      {
        response_list.add_response(response);
        break;
      }

    }

    // Allreduce, Allgather, Hipressgradalltoall are using nccl as backend, so they are executed in non-blocking thread.
    // Hipressgather, Hipressbroadcast and Hipressalltoallv(fused by last two) are using mpi as backend, so they are executed in an independent blocking thread.
    // TODO: add backend info in requeset/response, devide them into different queues according to backend in FuseResponses step.
  }

  if (ar_response.tensor_names().size() > 0) {
    response_list.add_response(ar_response);
  }
  if (ag_response.tensor_names().size() > 0) {
    response_list.add_response(ag_response);
  }
  if (aa_response.tensor_names().size() > 0) {
    response_list.add_response(aa_response);
  }
  if (aab_response.tensor_names().size() > 0) {
    response_list.add_response(aab_response);
  }
  // LOG(DEBUG) << CommunicatorName(communicator_) <<  ": Gather count: " << gather_count << " Broadcast count: " << broadcast_count;
  
  // Not fusion Gather and Broadcast whose root_ranks are not same 

  // for (int i=0; i<GetCommSize(); ++i) {
  //   auto& gather_response = gather_vector(i);
  //   while (!gather_response.empty()) {
  //     auto resp = gather_response.back();
  //     gather_response.pop_back();
  //     response_list.add_response(resp);
  //     gather_count--;
  //   }
  //   auto& broadcast_response = broadcast_vector(i);
  //   while (!broadcast_response.empty()) {
  //     auto resp = broadcast_response.back();
  //     broadcast_response.pop_back();
  //     response_list.add_response(resp);
  //     broadcast_count--;
  //   }
  // }

  // Fuse Gather and Broadcast to alltoall respectively.

  int mpi_index = response_list.responses().size();
  response_list.set_mpi_index(mpi_index);

  while (gather_count > 0) {
      Response alltoall_response;
      Response resp;
      bool is_broadcast = false;
      int alltoall_response_count = 0;
      for (int i=0; i<GetCommSize(); ++i) {
        auto& gather_response = gather_vector(i);
        if (!gather_response.empty()) {
          resp = gather_response.back();
          gather_response.pop_back();
          alltoall_response.add_alltoall_response(resp, is_broadcast);
          gather_count--;
          alltoall_response_count++;
        }
      }
      if (alltoall_response_count > 1) {
        response_list.add_response(alltoall_response);
      } else {
        response_list.add_response(resp);
      }
  }
  while (broadcast_count > 0) {
    Response alltoall_response;
    Response resp;
    bool is_broadcast = true;
    int alltoall_response_count = 0;
    for (int i=0; i<GetCommSize(); ++i) {
      auto& broadcast_response = broadcast_vector(i);
      if (!broadcast_response.empty()) {
        resp = broadcast_response.back();
        broadcast_response.pop_back();
        alltoall_response.add_alltoall_response(resp, is_broadcast);
        broadcast_count--;
        alltoall_response_count++;
      }
    }
    if (alltoall_response_count > 1) {
      response_list.add_response(alltoall_response);
    } else {
      response_list.add_response(resp);
    }
  }

  assert(gather_count == 0 && broadcast_count == 0);
  if (!response_list.responses().empty()) {
    std::string fusion_result;
    for (auto& re : response_list.responses()) {
      fusion_result += re.tensor_names_string() + ";";
    }
    LOG(DEBUG) << "Fusion result: " << fusion_result;
  } 

  if (response_alltoallv.tensor_names().size() > 0) {
    LOG(DEBUG) << "Fusion alltoallv result: " << response_alltoallv.tensor_names_string();
  }

  return response_list;
}

int64_t Controller::TotalByteSizeOfAllgatherOutput(
    const std::vector<int64_t>& tensor_sizes, const TensorTableEntry& entry) {
  int64_t total_dimension_size = 0;
  for (auto sz : tensor_sizes) {
    total_dimension_size += sz;
  }
  // Every tensor participating in Allgather operation may have
  // different first dimension size, but the rest of dimensions are same
  // for all tensors.  Here we get shape of tensor sliced by first
  // dimension. Allgather output will have shape of: (sum of first
  // dimension of every tensor) x (tensor slice shape).
  int64_t total_count_of_output_entries = total_dimension_size;
  for (int i = 1; i < entry.tensor->shape().dims(); ++i) {
    total_count_of_output_entries *= entry.tensor->shape().dim_size(i);
  }
  int element_size = GetTypeSize(entry.tensor->dtype());
  int64_t total_byte_size_of_output =
      total_count_of_output_entries * element_size;

  return total_byte_size_of_output;
}

int Controller::GetLocalSizeAtCrossRank(int i) {
  return local_sizes_for_cross_rank_[i];
}

bool Controller::IncrementTensorCount(const Request& msg, int joined_size) {
  auto& name = msg.tensor_name();
  auto table_iter = message_table_.find(name);
  if (table_iter == message_table_.end()) {
    std::vector<Request> messages = {msg};
    messages.reserve(static_cast<unsigned long>(GetCommSize()));
    message_table_.emplace(name, std::move(messages));
    table_iter = message_table_.find(name);
    timeline_.NegotiateStart(name, msg.request_type());
  } else {
    std::vector<Request>& messages = table_iter->second;
    messages.push_back(msg);
  }

  timeline_.NegotiateRankReady(name, msg.request_rank());

  std::vector<Request>& messages = table_iter->second;
  int count = (int)messages.size();
  bool ready_to_reduce = count == (GetCommSize() - joined_size);
  if (ready_to_reduce) {
    timeline_.NegotiateEnd(name);
  }
  return ready_to_reduce;
}

void Controller::SetTimelineEnabled(bool value) {
  std::lock_guard<std::recursive_mutex> guard(timeline_mutex_);
  timeline_enabled_pending_ = value;
  timeline_enabled_ = value;
}

void Controller::SetTimelineEnabledPending(bool value) {
  std::lock_guard<std::recursive_mutex> guard(timeline_mutex_);
  timeline_enabled_pending_ = value;
}

void Controller::SetMarkCyclesInTimelinePending(bool value) {
  std::lock_guard<std::recursive_mutex> guard(timeline_mutex_);
  mark_cycles_in_timeline_pending_ = value;
}

void Controller::SynchronizeTimelineEnabled() {
  std::lock_guard<std::recursive_mutex> guard(timeline_mutex_);
  timeline_enabled_ = timeline_enabled_pending_;
}

bool Controller::TimeLineEnabled() {
  std::lock_guard<std::recursive_mutex> guard(timeline_mutex_);
  return timeline_enabled_;
}

bool Controller::TimelineEnabledPending() {
  std::lock_guard<std::recursive_mutex> guard(timeline_mutex_);
  return timeline_enabled_pending_;
}

bool Controller::MarkCyclesInTimelinePending() {
  std::lock_guard<std::recursive_mutex> guard(timeline_mutex_);
  return mark_cycles_in_timeline_pending_;
}
} // namespace common
} // namespace horovod
