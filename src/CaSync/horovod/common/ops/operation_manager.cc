// Copyright 2019 Uber Technologies, Inc. All Rights Reserved.
// Modifications copyright Microsoft
// Modifications copyright (C) 2020, NVIDIA CORPORATION. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================

#include "operation_manager.h"

namespace horovod {
namespace common {

OperationManager::OperationManager(ParameterManager* param_manager,
                                   std::vector<std::shared_ptr<AllreduceOp>> allreduce_ops,
                                   std::vector<std::shared_ptr<AllgatherOp>> allgather_ops,
                                   std::vector<std::shared_ptr<BroadcastOp>> broadcast_ops,
                                   std::vector<std::shared_ptr<AlltoallOp>> alltoall_ops,
                                   std::vector<std::shared_ptr<LocalReduceOp>> hipressreduce_ops,
                                   std::vector<std::shared_ptr<BroadcastOp>> hipressbroadcast_ops,
                                   std::vector<std::shared_ptr<GatherOp>> hipressgather_ops,
                                   std::vector<std::shared_ptr<AlltoallvOp>> hipressalltoallv_ops,
                                   std::vector<std::shared_ptr<GradAlltoallOp>> hipressgradalltoall_ops,
                                   std::vector<std::shared_ptr<GradAlltoallBcastOp>> hipressgradalltoallbcast_ops,
                                   std::vector<std::shared_ptr<GCLAlltoallvOp>> gcl_alltoallv_ops,
                                   std::shared_ptr<JoinOp> join_op,
                                   std::vector<std::shared_ptr<AllreduceOp>> adasum_ops,
                                   std::shared_ptr<ErrorOp> error_op)
    : param_manager_(param_manager),
      allreduce_ops_(std::move(allreduce_ops)),
      allgather_ops_(std::move(allgather_ops)),
      broadcast_ops_(std::move(broadcast_ops)),
      alltoall_ops_(std::move(alltoall_ops)),
      hipressreduce_ops_(std::move(hipressreduce_ops)),
      hipressbroadcast_ops_(std::move(hipressbroadcast_ops)),
      hipressgather_ops_(std::move(hipressgather_ops)),
      hipressalltoallv_ops_(std::move(hipressalltoallv_ops)),
      hipressgradalltoall_ops_(std::move(hipressgradalltoall_ops)),
      hipressgradalltoallbcast_ops_(std::move(hipressgradalltoallbcast_ops)),
      gcl_alltoallv_ops_(std::move(gcl_alltoallv_ops)),
      join_op_(std::move(join_op)),
      adasum_ops_(std::move(adasum_ops)),
      error_op_(std::move(error_op)){}

Status OperationManager::ExecuteAllreduce(std::vector<TensorTableEntry>& entries,
                                          const Response& response) const {
  for (auto& op : allreduce_ops_) {
    if (op->Enabled(*param_manager_, entries, response)) {
      return op->Execute(entries, response);
    }
  }
  throw std::logic_error("No Allreduce operation enabled");
}

Status OperationManager::ExecuteAllgather(std::vector<TensorTableEntry>& entries,
                                          const Response& response) const {
  for (auto& op : allgather_ops_) {
    if (op->Enabled(*param_manager_, entries, response)) {
      return op->Execute(entries, response);
    }
  }
  throw std::logic_error("No Allgather operation enabled");
}

Status OperationManager::ExecuteBroadcast(std::vector<TensorTableEntry>& entries,
                                          const Response& response) const {
  for (auto& op : broadcast_ops_) {
    if (op->Enabled(*param_manager_, entries, response)) {
      return op->Execute(entries, response);
    }
  }
  throw std::logic_error("No Broadcast operation enabled");
}

Status OperationManager::ExecuteAlltoall(std::vector<TensorTableEntry>& entries,
                                         const Response& response) const {
  for (auto& op : alltoall_ops_) {
    if (op->Enabled(*param_manager_, entries, response)) {
      return op->Execute(entries, response);
    }
  }
  throw std::logic_error("No Alltoall operation enabled");
}

Status OperationManager::ExecuteHIPRESSReduce(std::vector<TensorTableEntry>& entries,
                                              const Response& response) const {
  auto& e = entries[0];
  auto& op = hipressreduce_ops_[e.backend];
  if (op->Enabled(*param_manager_, entries, response)) {
    return op->Execute(entries, response);
  }
  throw std::logic_error("No HIPRESSReduce operation enabled");
}

Status OperationManager::ExecuteHIPRESSBroadcast(std::vector<TensorTableEntry>& entries,
                                         const Response& response) const {
  auto& e = entries[0];
  auto& op = hipressbroadcast_ops_[e.backend];
  if (op->Enabled(*param_manager_, entries, response)) {
    return op->Execute(entries, response);
  }
  throw std::logic_error("No HIPRESSBroadcast operation enabled");
}

Status OperationManager::ExecuteHIPRESSGather(std::vector<TensorTableEntry>& entries,
                                         const Response& response) const {
  auto& e = entries[0];
  auto& op = hipressgather_ops_[e.backend];
  if (op->Enabled(*param_manager_, entries, response)) {
    return op->Execute(entries, response);
  }
  throw std::logic_error("HIPRESSGather operation is not enabled");
}

Status OperationManager::ExecuteHIPRESSGradAlltoall(std::vector<TensorTableEntry>& entries,
                                         const Response& response) const {
  auto& e = entries[0];
  auto& op = hipressgradalltoall_ops_[e.backend];
  if (op->Enabled(*param_manager_, entries, response)) {
    return op->Execute(entries, response);
  }
  throw std::logic_error("HIPRESSGradAlltoall operation is not enabled");
}

Status OperationManager::ExecuteHIPRESSGradAlltoallBcast(std::vector<TensorTableEntry>& entries,
                                         const Response& response) const {
  auto& e = entries[0];
  auto& op = hipressgradalltoallbcast_ops_[e.backend];
  if (op->Enabled(*param_manager_, entries, response)) {
    return op->Execute(entries, response);
  }
  throw std::logic_error("HIPRESSGradAlltoall operation is not enabled");
}

Status OperationManager::ExecuteHIPRESSAlltoallv(std::vector<TensorTableEntry>& entries,
                                         const Response& response) const {
  auto& e = entries[0];
  auto& op = hipressalltoallv_ops_[e.backend];
  if (op->Enabled(*param_manager_, entries, response)) {
    return op->Execute(entries, response);
  }
  throw std::logic_error("HIPRESSAlltoallv operation is not enabled");
}

Status OperationManager::ExecuteJoin(std::vector<TensorTableEntry>& entries,
                                          const Response& response) const {
  return join_op_->Execute(entries, response);
}

Status OperationManager::ExecuteAdasum(std::vector<TensorTableEntry>& entries,
                                          const Response& response) const {
  for (auto& op : adasum_ops_) {
    if (op->Enabled(*param_manager_, entries, response)) {
      return op->Execute(entries, response);
    }
  }
  throw std::logic_error("No Adasum operation enabled");
}

Status OperationManager::ExecuteError(std::vector<TensorTableEntry>& entries,
                                      const Response& response) const {
  return error_op_->Execute(entries, response);
}

Status OperationManager::ExecuteOperation(std::vector<TensorTableEntry>& entries,
                                          const Response& response) const {
  if (response.response_type() == Response::ALLREDUCE) {
    return ExecuteAllreduce(entries, response);
  } else if (response.response_type() == Response::ALLGATHER) {
    return ExecuteAllgather(entries, response);
  } else if (response.response_type() == Response::BROADCAST) {
    return ExecuteBroadcast(entries, response);
  } else if (response.response_type() == Response::ALLTOALL) {
    return ExecuteAlltoall(entries, response);
  } else if (response.response_type() == Response::HIPRESSREDUCE) {
    return ExecuteHIPRESSReduce(entries, response);
  } else if (response.response_type() == Response::HIPRESSBROADCAST) {
    return ExecuteHIPRESSBroadcast(entries, response);
  } else if (response.response_type() == Response::HIPRESSGATHER) {
    return ExecuteHIPRESSGather(entries, response);
  } else if (response.response_type() == Response::HIPRESSGRADALLTOALL) {
    return ExecuteHIPRESSGradAlltoall(entries, response);
  } else if (response.response_type() == Response::HIPRESSGRADALLTOALLBCAST) {
    return ExecuteHIPRESSGradAlltoallBcast(entries, response);
  } else if (response.response_type() == Response::HIPRESSALLTOALLV) {
    return ExecuteHIPRESSAlltoallv(entries, response);
  }else if (response.response_type() == Response::JOIN) {
    return ExecuteJoin(entries, response);
  } else if (response.response_type() == Response::ADASUM) {
    return ExecuteAdasum(entries, response);
  } else if (response.response_type() == Response::ERROR) {
    return ExecuteError(entries, response);
  } else {
    throw std::logic_error("No operation found for response type provided");
  }
}

Status OperationManager::ExecuteOperation(std::unordered_map<std::string, TensorTableEntry>& entries_map, std::vector<TensorTableEntry>& entries, const ResponseAlltoallv& response) const {
  auto& e = entries[0];
  auto& op = gcl_alltoallv_ops_[e.backend];
  if (op->Enabled(*param_manager_, entries, response)) {
    return op->Execute(entries_map, entries, response);
  }
  throw std::logic_error("GCL_Alltoallv operation is not enabled");
}


} // namespace common
} // namespace horovod
