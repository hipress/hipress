// Copyright 2016 The TensorFlow Authors. All Rights Reserved.
// Modifications copyright (C) 2019 Uber Technologies, Inc.
// Modifications copyright (C) 2020, NVIDIA CORPORATION. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================

#include "collective_operations.h"
#include "../message.h"
#include "../logging.h"

namespace horovod {
namespace common {

HorovodOp::HorovodOp(HorovodGlobalState* global_state)
    : global_state_(global_state) {}

int64_t HorovodOp::NumElements(std::vector<TensorTableEntry>& entries) {
  int64_t num_elements = 0;
  for (auto& e : entries) {
    num_elements += e.tensor->shape().num_elements();
  }
  return num_elements;
}

void HorovodOp::MemcpytoBuffer(const TensorTableEntry& entry, void* target, const void* src, size_t msg_size){
  std::memcpy(target, src, msg_size);
} 

// Allreduce
AllreduceOp::AllreduceOp(HorovodGlobalState* global_state)
    : HorovodOp(global_state) {}

void AllreduceOp::MemcpyInFusionBuffer(
    const std::vector<TensorTableEntry>& entries, const void*& fused_input_data,
    void*& buffer_data, size_t& buffer_len) {
  // Access the fusion buffer.
  auto& first_entry = entries[0];
  auto buffer = global_state_->GetFusionBufferManager(first_entry.comm).GetBuffer(
      first_entry.device, first_entry.context->framework(), global_state_->GetNcclStream(first_entry.comm));
  buffer_data = const_cast<void*>(buffer->AccessData(first_entry.context));

  int64_t offset = 0;
  for (auto& e : entries) {
    void* buffer_data_at_offset = (uint8_t*)buffer_data + offset;
    MemcpyEntryInFusionBuffer(entries, e, buffer_data_at_offset);
    int tensor_size = 0;
    if (e.num_elem > 0){
      tensor_size = e.num_elem;
    } else {
      tensor_size = e.tensor->size();
    }
    offset += tensor_size;
  }

  buffer_len = (size_t)offset;

  // Set the input data to originate from the buffer.
  fused_input_data = buffer_data;
}

void AllreduceOp::MemcpyOutFusionBuffer(
    const void* buffer_data, std::vector<TensorTableEntry>& entries) {
  int64_t offset = 0;
  for (auto& e : entries) {
    void* buffer_data_at_offset = (uint8_t*)buffer_data + offset;
    MemcpyEntryOutFusionBuffer(entries, buffer_data_at_offset, e);
    int tensor_size = 0;
    if (e.num_elem > 0){
      tensor_size = e.num_elem;
    } else {
      tensor_size = e.tensor->size();
    }
    offset += tensor_size;
  }
}

void AllreduceOp::MemcpyEntryInFusionBuffer(
    const std::vector<TensorTableEntry>& entries, const TensorTableEntry& e,
    void* buffer_data_at_offset) {
  int tensor_size = 0;
  if (e.num_elem > 0){
    tensor_size = e.num_elem;
  } else {
    tensor_size = e.tensor->size();
  }
  std::memcpy(buffer_data_at_offset, e.tensor->data(),
              (size_t)tensor_size);
}

void AllreduceOp::MemcpyEntryOutFusionBuffer(
    const std::vector<TensorTableEntry>& entries,
    const void* buffer_data_at_offset, TensorTableEntry& e) {
  int tensor_size = 0;
  if (e.num_elem > 0){
    tensor_size = e.num_elem;
  } else {
    tensor_size = e.tensor->size();
  }
  std::memcpy((void*)e.output->data(), buffer_data_at_offset,
              (size_t)tensor_size);
}

void AllreduceOp::ScaleBuffer(
    double scale_factor, const std::vector<TensorTableEntry>& entries,
    const void* fused_input_data, void* buffer_data,
    int64_t num_elements) {

  DataType dtype = entries[0].tensor->dtype();
  switch (dtype) {
    case HOROVOD_UINT8:
      ScaleBufferCPUImpl((const uint8_t*) fused_input_data, (uint8_t*) buffer_data, num_elements, scale_factor);
      break;
    case HOROVOD_INT8:
      ScaleBufferCPUImpl((const int8_t*) fused_input_data, (int8_t*) buffer_data, num_elements, scale_factor);
      break;
    case HOROVOD_INT32:
      ScaleBufferCPUImpl((const int32_t*) fused_input_data, (int32_t*) buffer_data, num_elements, scale_factor);
      break;
    case HOROVOD_INT64:
      ScaleBufferCPUImpl((const int64_t*) fused_input_data, (int64_t*) buffer_data, num_elements, scale_factor);
      break;
    case HOROVOD_FLOAT16:
      ScaleBufferCPUImpl((const unsigned short*) fused_input_data, (unsigned short*) buffer_data, num_elements, (float) scale_factor);
      break;
    case HOROVOD_FLOAT32:
      ScaleBufferCPUImpl((const float*) fused_input_data, (float*) buffer_data, num_elements, (float) scale_factor);
      break;
    case HOROVOD_FLOAT64:
      ScaleBufferCPUImpl((const double*) fused_input_data, (double*) buffer_data, num_elements, scale_factor);
      break;
    default:
      throw std::logic_error("Type " + DataType_Name(dtype) +
                             " not supported by ScaleBufferCPUImpl.");
  }
}

// Allgather
AllgatherOp::AllgatherOp(HorovodGlobalState* global_state)
    : HorovodOp(global_state) {}

Status AllgatherOp::AllocateOutput(std::vector<TensorTableEntry>& entries,
                                   const Response& response,
                                   int64_t**& entry_component_sizes,
                                   int*& recvcounts) {
  int global_size = global_state_->controller->GetSize();
  for (unsigned int ec = 0; ec < entries.size(); ++ec) {
    auto& e = entries[ec];
    // Every tensor participating in Allgather operation may have different
    // first dimension size, but the rest of dimensions are same for all
    // tensors.  Here we get shape of tensor sliced by first dimension.
    TensorShape single_slice_shape;
    for (int i = 1; i < e.tensor->shape().dims(); ++i) {
      single_slice_shape.AddDim(e.tensor->shape().dim_size(i));
    }

    // Copy tensor sizes from the response into a vector of int64_t
    // and compute total size.  This is size of first dimension.
    int64_t total_entry_dimension_size = 0;
    const auto& tensor_sizes = response.tensor_sizes();
    for (int rc = 0; rc < global_size; ++rc) {
      auto component_size = tensor_sizes[ec * global_size + rc];
      total_entry_dimension_size += component_size;
      recvcounts[rc] += component_size * single_slice_shape.num_elements();
      entry_component_sizes[ec][rc] =
          component_size * single_slice_shape.num_elements();
    }

    // Allgather output will have shape of:
    // (sum of first dimension of every tensor) x (tensor slice shape).
    TensorShape output_shape;
    output_shape.AddDim((int64_t)total_entry_dimension_size);
    output_shape.AppendShape(single_slice_shape);

    Status status = e.context->AllocateOutput(output_shape, &e.output);
    if (!status.ok()) {
      return status;
    }
  }

  return Status::OK();
}

void AllgatherOp::SetDisplacements(const int* recvcounts, int*& displcmnts) {
  int global_size = global_state_->controller->GetSize();
  for (int rc = 0; rc < global_size; ++rc) {
    if (rc == 0) {
      displcmnts[rc] = 0;
    } else {
      displcmnts[rc] = displcmnts[rc - 1] + recvcounts[rc - 1];
    }
  }
}

void AllgatherOp::SetEntryComponentOffsets(
    const std::vector<TensorTableEntry>& entries,
    const int64_t* const* entry_component_sizes, const int* recvcounts,
    int64_t**& entry_component_offsets) {
  unsigned int rank_displacement = 0;
  int global_size = global_state_->controller->GetSize();
  for (int rc = 0; rc < global_size; ++rc) {
    for (unsigned int ec = 0; ec < entries.size(); ++ec) {
      if (ec == 0) {
        entry_component_offsets[ec][rc] = rank_displacement;
      } else {
        entry_component_offsets[ec][rc] = entry_component_offsets[ec - 1][rc] +
                                          entry_component_sizes[ec - 1][rc];
      }
    }
    rank_displacement += recvcounts[rc];
  }
}

void AllgatherOp::MemcpyInFusionBuffer(
    const std::vector<TensorTableEntry>& entries, const int* displcmnts,
    int element_size, void*& buffer_data) {
  // Access the fusion buffer.
  auto& first_entry = entries[0];
  auto buffer = global_state_->fusion_buffer.GetBuffer(
      first_entry.device, first_entry.context->framework(), global_state_->GetNcclStream(first_entry.comm));
  buffer_data = const_cast<void*>(buffer->AccessData(first_entry.context));

  int64_t offset = displcmnts[global_state_->controller->GetRank()] * element_size;
  for (auto& e : entries) {
    void* buffer_data_at_offset = (uint8_t*)buffer_data + offset;
    MemcpyEntryInFusionBuffer(entries, e, buffer_data_at_offset);
    offset += e.tensor->size();
  }
}

void AllgatherOp::MemcpyOutFusionBuffer(
    const int64_t* const* entry_component_offsets,
    const int64_t* const* entry_component_sizes, const void* buffer_data,
    int element_size, std::vector<TensorTableEntry>& entries) {
  // Copy memory out of the fusion buffer.
  int global_size = global_state_->controller->GetSize();
  for (unsigned int ec = 0; ec < entries.size(); ++ec) {
    auto& e = entries[ec];
    int64_t copy_offset = 0;
    for (int rc = 0; rc < global_size; ++rc) {
      int64_t entry_offset = entry_component_offsets[ec][rc] * element_size;
      int64_t entry_size = entry_component_sizes[ec][rc] * element_size;
      const void* buffer_data_at_offset = (uint8_t*)buffer_data + entry_offset;
      MemcpyEntryOutFusionBuffer(entries, buffer_data_at_offset, e,
                                 copy_offset, entry_size);
      copy_offset += entry_size;
    }
  }
}

void AllgatherOp::MemcpyEntryInFusionBuffer(
    const std::vector<TensorTableEntry>& entries, const TensorTableEntry& e,
    void* buffer_data_at_offset) {
  std::memcpy(buffer_data_at_offset, e.tensor->data(),
              (size_t)e.tensor->size());
}

void AllgatherOp::MemcpyEntryOutFusionBuffer(
    const std::vector<TensorTableEntry>& entries,
    const void* buffer_data_at_offset, TensorTableEntry& e,
    int64_t entry_offset, size_t entry_size) {
  std::memcpy((uint8_t*)e.output->data() + entry_offset,
              buffer_data_at_offset, entry_size);
}

LocalReduceOp::LocalReduceOp(HorovodGlobalState* global_state)
    : HorovodOp(global_state) {}

void LocalReduceOp::MemcpyInFusionBuffer(const std::vector<TensorTableEntry>& entries, void*& buffer_data, int element_size) {
  // Access the fusion buffer.
  auto& first_entry = entries[0];
  std::shared_ptr<Controller> controller = global_state_->GetController(first_entry.comm);

  auto buffer = global_state_->GetFusionBufferManager(first_entry.comm).GetSendBuffer(
      first_entry.device, first_entry.context->framework(), global_state_->GetNcclStream(first_entry.comm));
  buffer_data = const_cast<void*>(buffer->AccessData(first_entry.context));

  int64_t offset = 0;
  int64_t byte_of_tensor;
  for (auto& e : entries) {
    void* buffer_data_at_offset = (uint8_t*) buffer_data + offset;
    byte_of_tensor = e.num_elem * element_size;
    MemcpytoBuffer(first_entry, (uint8_t*) (buffer_data_at_offset), (uint8_t*) e.tensor->data(), (size_t) (byte_of_tensor));
    offset += byte_of_tensor;
  }
}

void LocalReduceOp::MemcpyOutFusionBuffer(const void* buffer_data, std::vector<TensorTableEntry>& entries, int element_size) {
  auto& first_entry = entries[0];
  int64_t offset = 0;
  int64_t byte_of_tensor;
  for (auto& e : entries) {
    void* buffer_data_at_offset = (uint8_t*) buffer_data + offset;
    byte_of_tensor = e.num_elem * element_size;
    MemcpytoBuffer(first_entry, (void*) ((uint8_t*) e.output->data()), buffer_data_at_offset, (size_t) byte_of_tensor);
    offset += byte_of_tensor;
  }
}

BroadcastOp::BroadcastOp(HorovodGlobalState* global_state)
    : HorovodOp(global_state) {}

void BroadcastOp::MemcpyInFusionBuffer(const std::vector<TensorTableEntry>& entries, const Response& response, void*& buffer_data, int element_size) {
  // Access the fusion buffer.
  auto& first_entry = entries[0];
  std::shared_ptr<Controller> controller = global_state_->GetController(first_entry.comm);
  int my_rank = controller->GetCommRank();
  int root_rank = response.root_rank(0);
  LOG(DEBUG) << "BroadcastOp::MemcpyInFusionBuffer: my_rank=" << my_rank << ", root_rank=" << root_rank;
  auto buffer = global_state_->GetFusionBufferManager(Communicator::MPI_OP).GetSendBuffer(
      first_entry.device, first_entry.context->framework(), global_state_->GetNcclStream(first_entry.comm));
  buffer_data = const_cast<void*>(buffer->AccessData(first_entry.context));
  if (my_rank == root_rank) {
    int64_t offset = 0;
    int64_t byte_of_tensor;
    for (auto& e : entries) {
      void* buffer_data_at_offset = (uint8_t*) buffer_data + offset;
      byte_of_tensor = e.num_elem * element_size;
      MemcpytoBuffer(first_entry, (uint8_t*) (buffer_data_at_offset), (uint8_t*) e.tensor->data(), (size_t) (byte_of_tensor));
      if (first_entry.backend == CommnicationBackend::MPI) {
        const int* ct_ip = (const int *)(e.tensor->data());
        LOG(DEBUG) << "BroadcastOp::MemcpyInFusionBuffer: " << e.tensor_name << " msg_size: " << *ct_ip << " offset: " << offset << " byte_of_tensor: " << byte_of_tensor;
      }
      offset += byte_of_tensor;
    }
  }
}

void BroadcastOp::MemcpyOutFusionBuffer(const void* buffer_data, std::vector<TensorTableEntry>& entries, int element_size) {
  auto& first_entry = entries[0];
  int64_t offset = 0;
  int64_t byte_of_tensor;
  for (auto& e : entries) {
    void* buffer_data_at_offset = (uint8_t*) buffer_data + offset;
    byte_of_tensor = e.num_elem * element_size;
    MemcpytoBuffer(first_entry, (void*) ((uint8_t*) e.output->data()), buffer_data_at_offset, (size_t) byte_of_tensor);
    if (first_entry.backend == CommnicationBackend::MPI) {
      const int* ct_ip = (const int *)(e.output->data());
      LOG(DEBUG) << "BroadcastOp::MemcpyOutFusionBuffer: " << e.tensor_name << " msg_size: " << *ct_ip << " offset: " << offset << " byte_of_tensor: " << byte_of_tensor;
    }
    offset += byte_of_tensor;
  }
}

GatherOp::GatherOp(HorovodGlobalState* global_state) : HorovodOp(global_state) {}

void GatherOp::MemcpyInFusionBuffer(const std::vector<TensorTableEntry>& entries, const Response& response, int element_size, void*& send_data, int& send_count, int* recv_counts, int* displcmnts) {
  // Access the fusion buffer.
  auto& first_entry = entries[0];
  std::shared_ptr<Controller> controller = global_state_->GetController(first_entry.comm);
  int my_rank = controller->GetCommRank();
  int root_rank = response.root_rank(0);
  int comm_size = controller->GetCommSize();
  auto buffer = global_state_->GetFusionBufferManager(Communicator::MPI_OP).GetSendBuffer(
      first_entry.device, first_entry.context->framework(), global_state_->GetNcclStream(first_entry.comm));
  send_data = const_cast<void*>(buffer->AccessData(first_entry.context));
  assert(response.root_ranks().size() == 1);
  int offset = 0;
  int64_t byte_of_tensor;
  if (my_rank != root_rank){
    for (auto e : entries) {
      byte_of_tensor = e.num_elem * element_size;
      const int* tensor_ip = (const int *)(e.tensor->data());
      LOG(DEBUG) << "GatherOp::MemcpyInFusionBuffer: " << e.tensor_name << " e.tensor->data(): " << *tensor_ip  << " offset: " << offset << " byte_of_tensor: " << byte_of_tensor;
      MemcpytoBuffer(first_entry, (uint8_t*) send_data + offset, (uint8_t*) e.tensor->data(), (size_t) (byte_of_tensor));
      offset += byte_of_tensor;
    }
  } else {
    for (int i = 0; i < comm_size; ++i) {
      recv_counts[i] = send_count;
      displcmnts[i] = i * send_count;
    }
    recv_counts[root_rank] = 0;
  }
}

void GatherOp::MemcpyOutFusionBuffer(std::vector<TensorTableEntry>& entries, const void* recv_data, int send_count, int element_size) {
  // Copy memory out of the fusion buffer.
  auto& first_entry = entries[0];
  int offset = 0;
  std::shared_ptr<Controller> controller = global_state_->GetController(first_entry.comm);
  int my_rank = controller->GetCommRank();
  int comm_size = controller->GetCommSize();
  for (auto& e: entries) {
    int byte_of_tensor = e.num_elem * element_size;
    int output_count = 0;
    for (int i=0; i<comm_size; ++i){
      if (i != my_rank){
        MemcpytoBuffer(first_entry,
                    (void*) ((uint8_t*) e.output->data() + output_count*byte_of_tensor),
                    (void*) ((uint8_t*) recv_data + i*send_count + offset),
                    (size_t) byte_of_tensor);
        const int* output_ip = (const int *)(e.output->data() + output_count*byte_of_tensor);
        LOG(DEBUG) << "GatherOp::MemcpyOutFusionBuffer: " << e.tensor_name << " e.output->data(): " << *output_ip << " offset: " << offset << " byte_of_tensor: " << byte_of_tensor;
        output_count++;
      }
    }
    LOG(DEBUG) << "GatherOp::MemcpyOutFusionBuffer: " << e.tensor_name << " output_count: " << output_count;
    offset += byte_of_tensor;
  }
}

AlltoallOp::AlltoallOp(HorovodGlobalState* global_state)
    : HorovodOp(global_state) {}

GradAlltoallOp::GradAlltoallOp(HorovodGlobalState* global_state): HorovodOp(global_state) {}

void GradAlltoallOp::MemcpyInFusionBuffer(const std::vector<TensorTableEntry>& entries, void*& send_data, void*& recv_data, int element_size, int64_t& send_byte_per_rank) {
  auto& first_entry = entries[0];
  std::shared_ptr<Controller> controller = global_state_->GetController(first_entry.comm);
  int my_rank = controller->GetCommRank();
  int comm_size = controller->GetCommSize();
  LOG(DEBUG) << "GradAlltoallOp::MemcpyInFusionBuffer: my_rank=" << my_rank;
  auto buffer = global_state_->GetFusionBufferManager(first_entry.comm).GetSendBuffer(
      first_entry.device, first_entry.context->framework(), global_state_->GetNcclStream(first_entry.comm));
  send_data = const_cast<void*>(buffer->AccessData(first_entry.context));
  auto recv_buffer = global_state_->GetFusionBufferManager(first_entry.comm).GetBuffer(
      first_entry.device, first_entry.context->framework(), global_state_->GetNcclStream(first_entry.comm));
  recv_data = const_cast<void*>(recv_buffer->AccessData(first_entry.context));

  int64_t buffer_offset = 0;
  for (auto& e : entries) {
    int64_t byte_of_tensor_slice = e.num_elem * element_size / comm_size;
    for (int i=0; i<comm_size; ++i) {
      if (my_rank == i) {
        continue;
      }
      void* buffer_data_at_offset = (uint8_t*) send_data + i * send_byte_per_rank + buffer_offset;
      void* tensor_data_at_offset = (uint8_t*) e.tensor->data() + i * byte_of_tensor_slice;
      MemcpytoBuffer(first_entry, buffer_data_at_offset, tensor_data_at_offset, byte_of_tensor_slice);
    }
    buffer_offset += byte_of_tensor_slice;
  }
}

void GradAlltoallOp::MemcpyOutFusionBuffer(const std::vector<TensorTableEntry>& entries, void*& buffer_data, int element_size, int64_t& send_byte_per_rank) {
  auto& first_entry = entries[0];
  std::shared_ptr<Controller> controller = global_state_->GetController(first_entry.comm);
  int my_rank = controller->GetCommRank();
  int comm_size = controller->GetCommSize();
  LOG(DEBUG) << "GradAlltoallOp::MemcpyOutFusionBuffer: my_rank=" << my_rank;
  
  int64_t buffer_offset = 0;
  for(auto& e : entries) {
    int64_t byte_of_tensor_slice = e.num_elem * element_size / comm_size;
    for (int i=0; i<comm_size; ++i) {
      if (my_rank == i) {
        continue;
      }
      void* buffer_data_at_offset = (uint8_t*) buffer_data + i * send_byte_per_rank + buffer_offset;
      void* tensor_data_at_offset = (uint8_t*) e.tensor->data() + i * byte_of_tensor_slice;
      MemcpytoBuffer(first_entry, tensor_data_at_offset, buffer_data_at_offset, byte_of_tensor_slice);
    }
    buffer_offset += byte_of_tensor_slice;
  }
}

GradAlltoallBcastOp::GradAlltoallBcastOp(HorovodGlobalState* global_state): HorovodOp(global_state) {}

void GradAlltoallBcastOp::MemcpyInFusionBuffer(const Response& response, const std::vector<TensorTableEntry>& entries, void*& send_data, void*& recv_data, int element_size, int64_t& send_byte_per_rank) {
  auto& first_entry = entries[0];
  std::shared_ptr<Controller> controller = global_state_->GetController(first_entry.comm);
  int my_rank = controller->GetCommRank();
  int comm_size = controller->GetCommSize();
  LOG(DEBUG) << "GradAlltoallBcastOp::MemcpyInFusionBuffer: my_rank=" << my_rank;
  auto buffer = global_state_->GetFusionBufferManager(first_entry.comm).GetSendBuffer(
      first_entry.device, first_entry.context->framework(), global_state_->GetNcclStream(first_entry.comm));
  send_data = const_cast<void*>(buffer->AccessData(first_entry.context));
  auto recv_buffer = global_state_->GetFusionBufferManager(first_entry.comm).GetBuffer(
      first_entry.device, first_entry.context->framework(), global_state_->GetNcclStream(first_entry.comm));
  recv_data = const_cast<void*>(recv_buffer->AccessData(first_entry.context));

  int64_t buffer_offset = 0;
  assert(entries.size() == response.tensor_sizes().size());
  for (int i=0; i<entries.size(); i++) {
    auto& e = entries[i];
    int64_t byte_of_tensor = response.tensor_sizes()[i] * element_size;
    void* buffer_data_at_offset = (uint8_t*) send_data + buffer_offset;
    void* tensor_data_at_offset = (uint8_t*) e.tensor->data();
      MemcpytoBuffer(first_entry, buffer_data_at_offset, tensor_data_at_offset, byte_of_tensor);
    buffer_offset += byte_of_tensor;
  }
}

void GradAlltoallBcastOp::MemcpyOutFusionBuffer(const Response& response, const std::vector<TensorTableEntry>& entries, void*& buffer_data, int element_size, int64_t& send_byte_per_rank) {
  auto& first_entry = entries[0];
  std::shared_ptr<Controller> controller = global_state_->GetController(first_entry.comm);
  int my_rank = controller->GetCommRank();
  int comm_size = controller->GetCommSize();
  LOG(DEBUG) << "GradAlltoallBcastOp::MemcpyOutFusionBuffer: my_rank=" << my_rank;
    
  int64_t buffer_offset = 0;
  assert(entries.size() == response.tensor_sizes().size());
  for (int i=0; i<entries.size(); i++) {
    auto& e = entries[i];
    int64_t byte_of_tensor = response.tensor_sizes()[i];
    int64_t byte_of_offset = e.tensor->shape().num_elements() * element_size / comm_size;
    LOG(DEBUG) << "GradAlltoallBcastOp::MemcpyOutFusionBuffer: byte_of_tensor=" << byte_of_tensor << ", byte_of_offset=" << byte_of_offset;
    for (int j=0; j<comm_size; ++j) {
      if (my_rank == j) {
        continue;
      }
      void* buffer_data_at_offset = (uint8_t*) buffer_data + j * send_byte_per_rank + buffer_offset;
      void* tensor_data_at_offset = (uint8_t*) e.tensor->data() + j * byte_of_offset;
      MemcpytoBuffer(first_entry, tensor_data_at_offset, buffer_data_at_offset, byte_of_tensor);
    }
    buffer_offset += byte_of_tensor;
  }
}

AlltoallvOp::AlltoallvOp(HorovodGlobalState* global_state) : HorovodOp(global_state) {}

void AlltoallvOp::MemcpyInFusionBuffer(const std::vector<TensorTableEntry>& entries,
const Response& response,
const int element_size,
void*& send_data,
int*& send_counts,
int*& send_displcmnts,
void*& recv_data,
int*& recv_counts,
int*& recv_displcmnts,
std::vector<std::tuple<int, bool, int> >& recv_infos) {
  int response_count_idx = 0;
  auto& first_entry = entries[0];
  int fusion_offset = 0;
  std::shared_ptr<Controller> controller = global_state_->GetController(first_entry.comm);
  int my_rank = controller->GetCommRank();
  int comm_size = controller->GetCommSize();
  auto send_buffer = global_state_->GetFusionBufferManager(Communicator::MPI_OP).GetSendBuffer(first_entry.device, first_entry.context->framework(), global_state_->GetNcclStream(first_entry.comm));
  send_data = const_cast<void*>(send_buffer->AccessData(first_entry.context));
  auto recv_buffer = global_state_->GetFusionBufferManager(Communicator::MPI_OP).GetBuffer(first_entry.device, first_entry.context->framework(), global_state_->GetNcclStream(first_entry.comm));
  recv_data = const_cast<void*>(recv_buffer->AccessData(first_entry.context));

  // Example: response's
  // root_ranks:         0      -2      2     3
  // entries_counts:     2       1      1     3
  // tensor_names(sizes):a,b     x      c     d,e,f
  // means: 2 tensor(a,b) gather to rank(0), 1 tensor(x) broadcast from rank(1), 1 tensor(c) gather to rank(2), 3 tensor(d,e,f) gather to rank(3)
  // Attention: response.root_ranks() is in  ascending order

  std::string s;
  for (unsigned int i=0; i<response.root_ranks().size(); ++i) {
    s += std::to_string(response.root_ranks()[i]) + " ";
  }
  LOG(DEBUG) << "response.root_ranks(): " << s;
  s = "";
  for (unsigned int i=0; i<response.entries_counts().size(); ++i) {
    s += std::to_string(response.entries_counts()[i]) + " ";
  }
  LOG(DEBUG) << "response.entries_counts(): " << s;
  s = "";
  for (unsigned int i=0; i<response.tensor_sizes().size(); ++i) {
    s += std::to_string(response.tensor_sizes()[i]) + " ";
  }
  LOG(DEBUG) << "response.tensor_sizes(): " << s;

  for (unsigned int i=0; i<response.root_ranks().size(); ++i) {
    int root_rank = response.root_ranks()[i];
    int entries_count = response.entries_counts()[i];
    if (root_rank >= 0) { 
      // Gather
      for (int j=0; j<entries_count; ++j){
        auto byte_of_tensor = response.tensor_sizes()[response_count_idx]*element_size;
        if (my_rank == root_rank) {
        recv_infos.push_back(std::make_tuple(response_count_idx, false, byte_of_tensor));
          for (int k=0; k<comm_size; ++k) {
            if (k != root_rank) {
              recv_counts[k] += byte_of_tensor;
            }
          }
        } else {
          send_counts[root_rank] += byte_of_tensor;
          void* buffer_data_at_offset = (uint8_t*) send_data + fusion_offset;
          MemcpytoBuffer(first_entry, buffer_data_at_offset, entries[response_count_idx].tensor->data(), (size_t) byte_of_tensor);
          fusion_offset += byte_of_tensor;
        }
        response_count_idx++;
      }
    } else { 
      // Broadcast
      root_rank = -root_rank - 1;
      for (int j=0; j<entries_count; ++j){
        auto byte_of_tensor = response.tensor_sizes()[response_count_idx]*element_size;
        if (my_rank == root_rank) {
          for(int k=0; k<comm_size; ++k) {
            if (k != root_rank) {
              send_counts[k] += byte_of_tensor;
            }
          }
          void* buffer_data_at_offset = (uint8_t*) send_data + fusion_offset;
          MemcpytoBuffer(first_entry, buffer_data_at_offset, entries[response_count_idx].tensor->data(), (size_t) byte_of_tensor);
          fusion_offset += byte_of_tensor;
        } else {
          recv_infos.push_back(std::make_tuple(response_count_idx, true, byte_of_tensor));
          recv_counts[root_rank] += byte_of_tensor;
        }
        response_count_idx++;
      }
    }
  }
  for (int i=1; i<comm_size; ++i) {
    if (response.root_ranks()[0] >= 0) {
      // Gather
      send_displcmnts[i] = send_displcmnts[i-1] + send_counts[i-1]*element_size;
    } else {
      // Broadcast
      send_displcmnts[i] = 0;
    }
    recv_displcmnts[i] = recv_displcmnts[i-1] + recv_counts[i-1]*element_size;
  }
  std::string sc = "";
  std::string rc = "";
  std::string sd = "";
  std::string rd = "";
  for (int i=0; i<comm_size; ++i) {
    sc += std::to_string(send_counts[i]) + " ";
    rc += std::to_string(recv_counts[i]) + " ";
    sd += std::to_string(send_displcmnts[i]) + " ";
    rd += std::to_string(recv_displcmnts[i]) + " ";
  }
  LOG(DEBUG) << "send_counts: " << sc;
  LOG(DEBUG) << "recv_counts: " << rc;
  LOG(DEBUG) << "send_displcmnts: " << sd;
  LOG(DEBUG) << "recv_displcmnts: " << rd;
}

void AlltoallvOp::MemcpyOutFusionBuffer(std::vector<TensorTableEntry>& entries,
const Response& response,
const void* buffer_data,
std::vector<std::tuple<int, bool, int> >& recv_infos,
int*& recv_displcmnts) {
  std::shared_ptr<Controller> controller = global_state_->GetController(entries[0].comm);
  auto& first_entry = entries[0];
  int my_rank = controller->GetCommRank();
  int comm_size = controller->GetCommSize();
  int offset = 0;
  for (auto recv_info : recv_infos) {
    int entry_idx = std::get<0>(recv_info);
    bool is_broadcast = std::get<1>(recv_info);
    int byte_of_tensor = std::get<2>(recv_info); 
    if (is_broadcast) {
      MemcpytoBuffer(first_entry, (void*) ((uint8_t*) entries[entry_idx].output->data()), (void*) ((uint8_t*) buffer_data + offset), (size_t) byte_of_tensor);
      offset += byte_of_tensor;
    } else {
      int output_count = 0;
      for (int i=0; i<comm_size; ++i){
        if (i != my_rank) {
          MemcpytoBuffer(first_entry,
                        (void*) ((uint8_t*) entries[entry_idx].output->data() + output_count*byte_of_tensor),
                        (void*) ((uint8_t*) buffer_data + recv_displcmnts[i] + offset),
                        (size_t) byte_of_tensor);
          output_count++;
        }
      }
      offset += byte_of_tensor;
      LOG(DEBUG) << "Alltoallv_gather::MemcpyOutFusionBuffer: " << entries[entry_idx].tensor_name << " output_count: " << output_count;
    }
  }
}

GCLAlltoallvOp::GCLAlltoallvOp(HorovodGlobalState* global_state) : HorovodOp(global_state) {}

void GCLAlltoallvOp::MemcpyInFusionBuffer(std::unordered_map<std::string, TensorTableEntry>& entries_map,
    const ResponseAlltoallv& response,
    const int element_size,
    void*& send_data,
    int*& send_counts,
    int*& send_displcmnts,
    void*& recv_data,
    int*& recv_counts,
    int*& recv_displcmnts) {
      LOG(DEBUG) << "GCLAlltoallvOp::MemcpyInFusionBuffer";
}

void GCLAlltoallvOp::MemcpyOutFusionBuffer(std::unordered_map<std::string, TensorTableEntry>& entries_map,
    const ResponseAlltoallv& response,
    const int element_size,
    const void* recv_data,
    int*& recv_displcmnts){
      LOG(DEBUG) << "GCLAlltoallvOp::MemcpyOutFusionBuffer";
}

// Join
JoinOp::JoinOp(HorovodGlobalState* global_state) : HorovodOp(global_state) {}

Status JoinOp::Execute(std::vector<TensorTableEntry>& entries,
                       const Response& response) {
  assert(entries.size() == 0);
  if (global_state_->joined) {
    global_state_->tensor_queue.RemoveJoinTensor();
    global_state_->joined = false;
  }
  return Status::OK();
}

// Error
ErrorOp::ErrorOp(HorovodGlobalState* global_state) : HorovodOp(global_state) {}

Status ErrorOp::Execute(std::vector<TensorTableEntry>& entries, const Response& response) {
  return Status::PreconditionError(response.error_message());
}

} // namespace common
} // namespace horovod
