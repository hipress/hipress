import threading
from horovod.torch.mpi_ops import synchronize
from horovod.torch.mpi_ops import allreduce_async_
from horovod.torch.mpi_ops import gather_async_, cbroadcast_async_, get_all_get_all_finished_keys, gather_synchronize, cbroadcast_synchronize

import hp_cuda, torch, math, time, threading, queue


POWERSGD_ENCODE1 = 40
POWERSGD_ENCODE2 = 50
POWERSGD_DECODE = 60
HOROVOD_ALLREDUCE_TASK_ID = 30
TIME_INTERVAL = 1 / 10000

def _hp_cuda_submit_task(name, tensor, residual, B, C, is_root, task_id):
    if residual is None:
        hp_cuda.submit_task(name, tensor, B, C, is_root, task_id)
    else:
        hp_cuda.submit_task(name, tensor, residual, B, C, is_root, task_id)

def _hp_cuda_submit_task_powersgd(name, tensor, residual, M, P, Q, task_id):
    hp_cuda.submit_task(name, tensor, residual, M, P, Q, task_id)

def _malloc_space_xpu( cpu_size : int, gpu_size : int, cpu_type = torch.uint8, gpu_type = torch.uint8):
    _on_gpu = None
    _on_cpu = None
    if cpu_size > 0:
        _on_cpu = torch.zeros(cpu_size, dtype=cpu_type).pin_memory()

    if gpu_size > 0:
        if gpu_type == torch.uint8:
            _on_gpu = torch.cuda.ByteTensor(gpu_size)
        elif gpu_type == torch.float32:
            _on_gpu = torch.cuda.FloatTensor(gpu_size)
        else:
            raise ValueError("_malloc_space_xpu: unknown gpu_type: {}, only support torch.uint8 or torch.float32".format(gpu_type))

    return _on_cpu, _on_gpu

def _compute_auxiliary_size( size : int, algorithm_name : str, params : dict, hvd_size : int):
    if algorithm_name=='terngrad':
        data_per_byte = 8 / params['bitwidth']
        compressed_size =  10 + (size + data_per_byte - 1) // data_per_byte
        CPU_size = hvd_size * compressed_size
        GPU_size = compressed_size
        return int(compressed_size), int(CPU_size), int(GPU_size)
    elif algorithm_name == 'tbq':
        compressed_size = (size + 3) // 4
        CPU_size = hvd_size * compressed_size
        GPU_size = compressed_size
        return int(compressed_size), int(CPU_size), int(GPU_size)

    elif algorithm_name == 'graddrop':
        compressed_size = 4 + int( 2 * math.ceil( size * ( 1 - params['drop_ratio'] ) ) ) * 4
        CPU_size = hvd_size * compressed_size
        GPU_size = size * 4
        return int(compressed_size), int(CPU_size), int(GPU_size)

    else:
        raise ValueError("unknown algorithm_name:{}".format(algorithm_name))

#input_queue : (tensor, name, None)
#output_queue : (tensor, name, handle)
def allreduce_thread_loop_(input_queue, output_queue, device_id):

    torch.cuda.set_device(device_id)
    torch.set_num_threads(1)
    import time

    _allreduce_handle = dict()
    _allredude_set = dict()
    _allreduce_index = dict()


    last_cycle_time = time.time()
    while True:
        start_time = time.time()
        sleep_time = (last_cycle_time + TIME_INTERVAL - start_time) 
        if sleep_time > 0:
            time.sleep(sleep_time)
        last_cycle_time = time.time()

        while not input_queue.empty():
            r = input_queue.get()
            index, tensor, name = r
            handle = allreduce_async_(tensor, average=True, name=name)
            _allreduce_handle[name] = handle
            _allreduce_index[name] = index
            #output_queue.put((index, tensor, name, None))

        for name in get_all_get_all_finished_keys(30):
            tensor = synchronize(_allreduce_handle[name])
            output_queue.put((_allreduce_index[name], tensor, name, None))

#input_queue : (tensor, name, root_id)
#output_queue : (tensor, name, None)
def compression_thread_loop_(input_queue, output_queue, algorithm_name, alg_params, hvd_size, hvd_rank, device_id):
    #assert algorithm_name == 'terngrad'
    torch.cuda.set_device(device_id)
    torch.set_num_threads(1)

    _comp_cpu = dict()
    _comp_res = dict()
    _comp_gpu = dict()
    _comp_set = dict()
    _comp_compressed_size = dict()
    _param = dict()
    _gather_handles = dict()
    _cboardcast_handles = dict()
    _root_id = dict()
    _index = dict()

    is_need_residual = None

    if algorithm_name == 'tbq' or algorithm_name == 'graddrop':
        is_need_residual = True
    else:
        is_need_residual = False

    #init hp_cuda backthread
    hp_cuda.init(
        algorithm_name,
        alg_params,
        device_id,
        hvd_rank,
        hvd_size
    )

    __root = 0

    last_cycle_time = time.time()
    while True:
        start_time = time.time()
        sleep_time = (last_cycle_time + TIME_INTERVAL - start_time) 
        if sleep_time > 0:
            time.sleep(sleep_time)
        last_cycle_time = time.time()

        #root_gather_list = []
        while not input_queue.empty():
            r = input_queue.get()
            index, tensor, name = r

            if name not in _comp_set:
                compressed_size, CPU_size, GPU_size = _compute_auxiliary_size(
                    tensor.numel(),
                    algorithm_name, 
                    alg_params,
                    hvd_size
                )
                C, B = _malloc_space_xpu(CPU_size, GPU_size)
                if is_need_residual:
                    _comp_res[name] = torch.zeros(tensor.numel(), device=device_id)
                else:
                    _comp_res[name] = None
                _comp_cpu[name] = C
                _comp_gpu[name] = B
                _comp_compressed_size[name] = compressed_size

            _comp_set[name] = tensor
            _index[name] = index
            _root_id[name] = __root
            __root = (__root + 1) % hvd_size

            #is root
            if _root_id[name] == hvd_rank:
                handle = gather_async_(
                    _comp_cpu[name],
                    _comp_cpu[name],
                    _root_id[name],
                    num_elem = _comp_compressed_size[name],
                    name = name,
                    batchid=0
                )
                _gather_handles[name] = handle
            else:
                # 0 not root
                # 10 comp
                #hp_cuda.submit_task(name, _comp_set[name], _comp_gpu[name], _comp_cpu[name], 0, 10)
                _hp_cuda_submit_task(name, _comp_set[name], _comp_res[name], _comp_gpu[name], _comp_cpu[name], 0, 10)

        finished_comp_names = hp_cuda.getResults(10)

        #non_root gather
        for name in finished_comp_names:
            handle = gather_async_(
                _comp_cpu[name],
                _comp_cpu[name],
                _root_id[name],
                num_elem = _comp_compressed_size[name],
                name = name,
                batchid=0
            )
            _gather_handles[name] = handle

        #task_id = 10 mean gather task (this is for horovod not for hp_cuda comp)
        finished_gather_names = get_all_get_all_finished_keys(task_id=10)

        for name in finished_gather_names:

            _comp_cpu[name] = gather_synchronize(_gather_handles[name])

            #it's root
            if _root_id[name] == hvd_rank:
                #for root to run  D and C
                # 1 is root
                # 30 is for D and C
                #hp_cuda.submit_task(name, _comp_set[name], _comp_gpu[name], _comp_cpu[name], 1, 30)
                _hp_cuda_submit_task(name, _comp_set[name], _comp_res[name], _comp_gpu[name], _comp_cpu[name], 1, 30)
            else:
                #non_root broadcast
                handle = cbroadcast_async_(
                    _comp_cpu[name],
                    _root_id[name],
                    num_elem = _comp_compressed_size[name],
                    name = name,
                    batchid = 0
                )
                _cboardcast_handles[name] = handle


        finished_root_dc_names = hp_cuda.getResults(30)

        #root broadcast
        for name in finished_root_dc_names:
            handle = cbroadcast_async_(
                _comp_cpu[name],
                _root_id[name],
                num_elem = _comp_compressed_size[name],
                name = name,
                batchid = 0
            )
            _cboardcast_handles[name] = handle

        #task_id = 20 mean cbroadcast task
        finished_cbroadcast_names = get_all_get_all_finished_keys(task_id=20)

        for name in finished_cbroadcast_names:

            _comp_cpu[name] = cbroadcast_synchronize(_cboardcast_handles[name])
            #is root

            if _root_id[name] == hvd_rank:
                #root output
                output_queue.put((_index[name], _comp_set[name], name, None))

            else:
                #for non root to D
                #hp_cuda.submit_task(name, _comp_set[name], _comp_gpu[name], _comp_cpu[name], 0, 20)
                _hp_cuda_submit_task(name, _comp_set[name], _comp_res[name], _comp_gpu[name], _comp_cpu[name], 0, 20)

        finished_decomp_names = hp_cuda.getResults(20)


        #non_root finish and output
        for name in finished_decomp_names:
            output_queue.put((_index[name], _comp_set[name], name, None))

def _malloc_space_powersgd(tensor, low_rank, device_id):
    grad_numel = tensor.numel()
    grad_dtype = tensor.dtype
    interval = math.ceil(math.sqrt(grad_numel))

    _p = torch.zeros((interval, low_rank), dtype=grad_dtype, device=device_id)
    _q = torch.normal(0, 1, (interval, low_rank), dtype=grad_dtype, device=device_id)
    _resd = torch.zeros(grad_numel, dtype=grad_dtype, device=device_id)
    _M = torch.zeros((interval, interval), dtype=grad_dtype, device=device_id)

    return _p, _q, _resd, _M

def powersgd_allreduce_thread_loop_(device_id):
    global _allreduce_handles, _allreduce_indexes, _allreduce_input_queue
    torch.cuda.set_device(device_id)
    torch.set_num_threads(1)
    import time

    last_cycle_time = time.time()
    while True:
        start_time = time.time()
        sleep_time = (last_cycle_time + TIME_INTERVAL - start_time) 
        if sleep_time > 0:
            time.sleep(sleep_time)
        last_cycle_time = time.time()

        while not _allreduce_input_queue.empty():
            r = _allreduce_input_queue.get()
            index, tensor, name = r
            # print("submit no-comp allreduce", name)
            handle = allreduce_async_(tensor, average=True, name=name)
            _allreduce_handles[name] = handle
            _allreduce_indexes[name] = index

def powersgd_compression_thread_loop(alg_params, hvd_size, hvd_rank, device_id):
    global _allreduce_index, _allreduce_handles
    algorithm_name = 'powersgd'
    input_queue = _compression_input_queue
    output_queue = _output_queue

    torch.cuda.set_device(device_id)
    torch.set_num_threads(1)

    _comp_res = dict()
    _comp_p = dict()
    _comp_q = dict()
    _comp_M = dict()
    _comp_set = dict()
    _index = dict()

    low_rank = alg_params['matrix_approximation_rank']

    is_need_residual = True

    hp_cuda.init(
        algorithm_name,
        alg_params,
        device_id,
        hvd_rank,
        hvd_size
    )

    last_cycle_time = time.time()
    while True:
        start_time = time.time()
        sleep_time = (last_cycle_time + TIME_INTERVAL - start_time) 
        if sleep_time > 0:
            time.sleep(sleep_time)
        last_cycle_time = time.time()

        while not input_queue.empty():
            r = input_queue.get()
            index, tensor, name = r

            # memory initialize
            # print("malloc", name)
            if name not in _comp_set:
                _comp_p[name], _comp_q[name], _comp_res[name], _comp_M[name] = \
                    _malloc_space_powersgd(tensor, low_rank, device_id)

            _comp_set[name] = tensor
            _index[name] = index

            # submit encode1 task
            # print("submit encode1", name)
            _hp_cuda_submit_task_powersgd(name, _comp_set[name], _comp_res[name], _comp_M[name], _comp_p[name], _comp_q[name], POWERSGD_ENCODE1)

        # do allreduce after encode1
        finished_encode1_names = hp_cuda.getResults(POWERSGD_ENCODE1)
        for name in finished_encode1_names:
            new_name = name + '_p'
            # print("submit allreduce1", new_name)
            handle = allreduce_async_(_comp_p[name], average=True, name=new_name)
            _allreduce_handles[new_name] = handle

        # do allreduce after encode2
        finished_encode2_names = hp_cuda.getResults(POWERSGD_ENCODE2)
        for name in finished_encode2_names:
            new_name = name + '_q'
            # print("submit allreduce2", new_name)
            handle = allreduce_async_(_comp_q[name], average=True, name=new_name)
            _allreduce_handles[new_name] = handle

        # get names of allreduce1 results
        finished_allreduce_names = get_all_get_all_finished_keys(task_id=HOROVOD_ALLREDUCE_TASK_ID)
        for new_name in finished_allreduce_names:
            if new_name[-2:] == '_p':
                # allreduce 1
                name = new_name[:-2]
                # print("finished allreduce1", new_name)
                _comp_p[name] = synchronize(_allreduce_handles[new_name])
                # print("submit encode2", name)
                _hp_cuda_submit_task_powersgd(name, _comp_set[name], _comp_res[name], _comp_M[name], _comp_p[name], _comp_q[name], POWERSGD_ENCODE2)
            elif new_name[-2:] == '_q':
                # allreduce 2
                name = new_name[:-2]
                # print("finished allreduce2", new_name)
                _comp_q[name] = synchronize(_allreduce_handles[new_name])
                # print("submit decode", name)
                _hp_cuda_submit_task_powersgd(name, _comp_set[name], _comp_res[name], _comp_M[name], _comp_p[name], _comp_q[name], POWERSGD_DECODE)
            else:
                # allreduce for grads < threshold
                # print("finished no-comp allreduce", name)
                tensor = synchronize(_allreduce_handles[name])
                output_queue.put((_allreduce_index[name], tensor, name, None))

        # output decode results
        finished_decode_names = hp_cuda.getResults(POWERSGD_DECODE)
        for name in finished_decode_names:
            output_queue.put((_index[name], _comp_set[name], name, None))

def create_and_start_powersgd_allreduce_thread(device_id):
    global _allreduce_thread
    if _allreduce_thread is not None:
        return
    _allreduce_thread = threading.Thread(
        target=powersgd_allreduce_thread_loop_,
        args=(device_id,)
    )
    _allreduce_thread.daemon = True
    _allreduce_thread.start()

def create_and_start_powersgd_compression_thread(alg_params, size, rank, device_id):
    global _compression_thread
    if _compression_thread is not None:
        return
    _compression_thread = threading.Thread(
        target=powersgd_compression_thread_loop,
        args=(alg_params, size, rank, device_id)
    )
    _compression_thread.daemon = True
    _compression_thread.start()

def init_threads(algorithm_name, alg_params, size, rank, device_id):
    global _initialized
    if _initialized:
        return
    _initialized = True
    if algorithm_name == 'powersgd':
        create_and_start_powersgd_allreduce_thread(device_id)
        create_and_start_powersgd_compression_thread(alg_params, size, rank, device_id)
    else:
        raise NotImplementedError()

def get_allreduce_input_queue():
    assert _initialized, "init GradCompEngine first!"
    return _allreduce_input_queue

def get_compression_input_queue():
    assert _initialized, "init GradCompEngine first!"
    return _compression_input_queue

def get_output_queue():
    assert _initialized, "init GradCompEngine first!"
    return _output_queue


_initialized = False

_allreduce_thread = None
_allreduce_handles = dict()
_allreduce_indexes = dict()

_allreduce_input_queue = queue.Queue()

_compression_thread = None

_compression_input_queue = queue.Queue()

_output_queue = queue.Queue()
