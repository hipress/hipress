# Copyright 2019 Uber Technologies, Inc. All Rights Reserved.
# Modifications copyright Microsoft
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

import os
import warnings

from contextlib import contextmanager

import torch

from horovod.torch.compression import Compression
from horovod.torch.mpi_ops import allreduce_async_
from horovod.torch.mpi_ops import synchronize
from horovod.torch.mpi_ops import size
from horovod.torch.mpi_ops import Average, Adasum, Sum
from horovod.torch.mpi_ops import rocm_built
from horovod.torch.mpi_ops import size, local_size, rank, local_rank

from horovod.torch.gradcomp.engine import allreduce_thread_loop_, compression_thread_loop_
from horovod.torch.gradcomp.engine import powersgd_allreduce_thread_loop_, powersgd_compression_thread_loop
import horovod.torch.gradcomp.engine as GradCompEngine

import queue
import threading
import time


MP_STATUS_CHECK_INTERVAL = 5.0

class _DistributedOptimizer(torch.optim.Optimizer):
    def __init__(self, params, named_parameters, compression,
                 backward_passes_per_step=1, op=Average,
                 gradient_predivide_factor=1.0, **kwargs):
        super(self.__class__, self).__init__(params)
        self._compression = compression

        if named_parameters is not None:
            named_parameters = list(named_parameters)
        else:
            named_parameters = [('allreduce.noname.%s' % i, v)
                                for param_group in self.param_groups
                                for i, v in enumerate(param_group['params'])]
        # make sure that named_parameters are tuples
        if any([not isinstance(p, tuple) for p in named_parameters]):
            raise ValueError('named_parameters should be a sequence of '
                             'tuples (name, parameter), usually produced by '
                             'model.named_parameters().')

        dups = _DistributedOptimizer.find_duplicates([k for k, _ in named_parameters])
        if len(dups) > 0:
            raise ValueError('Parameter names in named_parameters must be unique. '
                             'Found duplicates: %s' % ', '.join(dups))
        all_param_ids = {id(v)
                         for param_group in self.param_groups
                         for v in param_group['params']}
        named_param_ids = {id(v) for k, v in named_parameters}
        unnamed_param_ids = all_param_ids - named_param_ids
        if len(unnamed_param_ids):
            raise ValueError('named_parameters was specified, but one or more model '
                             'parameters were not named. Python object ids: '
                             '%s' % ', '.join(str(id) for id in unnamed_param_ids))
        if len(named_parameters) > 0:
            self._parameter_names = {v: k for k, v in sorted(named_parameters)}
        else:
            self._parameter_names = {v: 'allreduce.noname.%s' % i
                                     for param_group in self.param_groups
                                     for i, v in enumerate(param_group['params'])}

        self._name_parameters = { v : k for k, v in self._parameter_names.items() }

        self.backward_passes_per_step = backward_passes_per_step
        self._allreduce_delay = {v: self.backward_passes_per_step
                                 for _, v in sorted(named_parameters)}
        self.op = op
        self.gradient_predivide_factor = gradient_predivide_factor
        self._handles = {}
        self._grad_accs = []
        self._requires_update = set()
        self._split_tensor_dict = {}
        self._rev_split_tensor_dict = {}
        self._split_tensor_to_be_finish = {}
        self._compression_threshold = kwargs['threshold']
        self._partition_threshold = kwargs['partition_threshold']

        print("compression threshold:", self._compression_threshold)
        print("partition threshold:", self._partition_threshold)

        self.algorithm_name = kwargs['algorithm_name']
        self.algorithm_params = kwargs['algorithm_params']

        if self.algorithm_name == 'tbq':
            if 'threshold' not in self.algorithm_params:
                print("for tbq need parameter threshold")
        elif self.algorithm_name == 'terngrad':
            if 'enable_random' not in self.algorithm_params or 'bitwidth' not in self.algorithm_params:
                print("for terngrad need parameter enable_random and bitwidth")
        elif self.algorithm_name == 'graddrop':
            if 'sample_rate' not in self.algorithm_params or 'drop_ratio' not in self.algorithm_params:
                print("for terngrad need parameter sample_rate and drop_ratio")
        elif self.algorithm_name == 'powersgd':
            if 'matrix_approximation_rank' not in self.algorithm_params:
                print("for powersgd need matrix_approximation_rank")
        else:
            print("Not support compression algorithm!!! only support TBQ, terngrad, graddrop and powersgd")
            raise ValueError

        self._size = size()
        self._rank = rank()
        self._device_id = torch.cuda.current_device()
        self._synchronized = False
        self._should_synchronize = True

        if self.__class__.__name__ == 'SGD':
            self._sgd_parameter = { p : (groups['weight_decay'], groups['momentum'], groups['dampening'], groups['nesterov'], groups['lr'])
                for groups in self.param_groups
                for p in groups['params']
            }

        if size() > 1 or os.environ.get('HOROVOD_ELASTIC') == '1':
            if self.algorithm_name == 'powersgd':
                GradCompEngine.init_threads(self.algorithm_name, self.algorithm_params, self._size, self._rank, self._device_id)
                self._allreduce_input_queue = GradCompEngine.get_allreduce_input_queue()
                self._compression_input_queue = GradCompEngine.get_compression_input_queue()
                self._output_queue = GradCompEngine.get_output_queue()
            else:
                self._allreduce_input_queue = queue.Queue()
                self._compression_input_queue = queue.Queue()
                self._output_queue = queue.Queue()
                self._allreduce_thread = threading.Thread(
                    target=allreduce_thread_loop_,
                    args=(self._allreduce_input_queue, self._output_queue, self._device_id)
                )
                self._compression_thread = threading.Thread(
                    target=compression_thread_loop_,
                    args=(
                        self._compression_input_queue, self._output_queue,
                        self.algorithm_name, self.algorithm_params, self._size,
                    self.algorithm_name, self.algorithm_params, self._size,
                        self.algorithm_name, self.algorithm_params, self._size,
                        self._rank, self._device_id
                    )
                )
                self._allreduce_thread.daemon = True
                self._allreduce_thread.start()
                self._compression_thread.daemon = True
                self._compression_thread.start()

            # final
            self._register_hooks()
            #wait init
            time.sleep(5)


    def load_state_dict(self, *args, **kwargs):
        self._handles = {}
        self._synchronized = False
        self._should_synchronize = True
        for p in self._allreduce_delay:
            self._allreduce_delay[p] = self.backward_passes_per_step
        super(self.__class__, self).load_state_dict(*args, **kwargs)

    @staticmethod
    def find_duplicates(lst):
        seen = set()
        dups = set()
        for el in lst:
            if el in seen:
                dups.add(el)
            seen.add(el)
        return dups

    def set_backward_passes_per_step(self, passes):
        self.backward_passes_per_step = passes
        for p in self._allreduce_delay:
            self._allreduce_delay[p] = self.backward_passes_per_step

    def _register_hooks(self):
        for param_group in self.param_groups:
            for p in param_group['params']:
                if p.requires_grad:
                    p.grad = p.data.new(p.size()).zero_()
                    self._requires_update.add(p)
                    p_tmp = p.expand_as(p)
                    grad_acc = p_tmp.grad_fn.next_functions[0][0]
                    grad_acc.register_hook(self._make_hook(p))
                    self._grad_accs.append(grad_acc)

    def _allreduce_grad_async(self, p):
        name = self._parameter_names.get(p)
        tensor = p.grad
        tensor_compressed, ctx = self._compression.compress(tensor)
        if self.op == Average:
           # Split average operation across pre/postscale factors
           # C++ backend will apply additional 1 / size() factor to postscale_factor for op == Average.
            prescale_factor = 1.0 / self.gradient_predivide_factor
            postscale_factor = self.gradient_predivide_factor
        else:
            prescale_factor = 1.0
            postscale_factor = 1.0

        handle = allreduce_async_(tensor_compressed, name=name, op=self.op,
                                  prescale_factor=prescale_factor,
                                  postscale_factor=postscale_factor)
        return handle, ctx

    def _submit_task(self, p):
        name = self._parameter_names.get(p)
        tensor = p.grad
        numel = tensor.numel()

        if self._compression_threshold is None or numel < self._compression_threshold:
            r = (-1, tensor, name)
            self._allreduce_input_queue.put(r)
        elif numel > self._partition_threshold:

            if name in self._split_tensor_dict or name in self._split_tensor_to_be_finish:
                raise(ValueError)

            number_to_split = self._size

            tensor_after_split = torch.chunk(tensor, number_to_split, dim=0)
            for i in range(number_to_split):
                task_name = name + "+" +  str(i)
                r = (i, tensor_after_split[i], task_name)
                self._compression_input_queue.put(r)
                self._rev_split_tensor_dict[task_name] = name

            self._split_tensor_to_be_finish[name] = number_to_split
            self._split_tensor_dict[name] = [None] * number_to_split
        else:
            r = (-1, tensor, name)
            self._compression_input_queue.put(r)

        return True



    def _make_hook(self, p):
        def hook(*ignore):
            if p in self._handles and self._handles[p] is not None:
                if self._allreduce_delay[p] <= 0:
                    raise AssertionError(
                        "Gradients were computed more than "
                        "backward_passes_per_step times before call "
                        "to step(). Increase backward_passes_per_step to "
                        "accumulate gradients locally.")
            assert not p.grad.requires_grad
            assert self._allreduce_delay[p] > 0
            handle = None
            self._allreduce_delay[p] -= 1
            if self._allreduce_delay[p] == 0:
                handle = self._submit_task(p)
            self._handles[p] = handle
        return hook


    def synchronize(self):
        missing_p = self._requires_update - set(self._handles.keys())
        for p in missing_p:
            handle = self._submit_task(p)
            print("horovod::torch::_DistributedOptimizer::synchronize::loop1::p,handle,compress_status:",handle,compress_status)
            self._handles[p] = handle

        for p, handle in self._handles.items():
            if handle is None:
                handle = self._submit_task(p)
                print("horovod::torch::_DistributedOptimizer::synchronize::loop2::p,handle,ctx:",handle,compress_status)
                self._handles[p] = handle

        finish_count = 0
        submit_count = len(self._handles)
        while finish_count < submit_count:
            try:
                r = self._output_queue.get(timeout=MP_STATUS_CHECK_INTERVAL)
            except queue.Empty:
                continue

            index, tensor, name, handle = r
            if index >= 0:
                original_name = self._rev_split_tensor_dict[name]
                self._split_tensor_to_be_finish[original_name] -= 1
                self._split_tensor_dict[original_name][index] = tensor
                if self._split_tensor_to_be_finish[original_name] == 0:
                    tensor = torch.cat(self._split_tensor_dict[original_name], dim=0)
                    name = original_name
                else:
                    continue
            p = self._name_parameters.get(name)
            if handle is not None:
                tensor = synchronize(handle)

            self._allreduce_delay[p] = self.backward_passes_per_step
            p.grad.set_(tensor)
            finish_count += 1

        self._handles.clear()
        self._split_tensor_dict.clear()
        self._split_tensor_to_be_finish.clear()
        self._rev_split_tensor_dict.clear()

        self._synchronized = True

    @contextmanager
    def skip_synchronize(self):
        """
        A context manager used to specify that optimizer.step() should
        not perform synchronization.

        It's typically used in a following pattern:

        .. code-block:: python

            optimizer.synchronize()
            with optimizer.skip_synchronize():
                optimizer.step()
        """
        self._should_synchronize = False
        try:
            yield
        finally:
            self._should_synchronize = True

    def step(self, closure=None):
        if self._should_synchronize:
            if self._synchronized:
                warnings.warn("optimizer.step() called without "
                              "optimizer.skip_synchronize() context after "
                              "optimizer.synchronize(). This can cause training "
                              "slowdown. You may want to consider using "
                              "optimizer.skip_synchronize() context if you use "
                              "optimizer.synchronize() in your code.")
            if self.__class__.__name__ == 'SGD' and size() > 1:
                missing_p = self._requires_update - set(self._handles.keys())
                for p in missing_p:
                    handle = self._submit_task(p)
                    print("horovod::torch::_DistributedOptimizer::synchronize::loop1::p,handle,compress_status:",handle,compress_status)
                    self._handles[p] = handle

                for p, handle in self._handles.items():
                    if handle is None:
                        handle = self._submit_task(p)
                        print("horovod::torch::_DistributedOptimizer::synchronize::loop2::p,handle,ctx:",handle,compress_status)
                        self._handles[p] = handle

                finish_count = 0
                submit_count = len(self._handles)

                loss = None
                if closure is not None:
                    with torch.enable_grad():
                        loss = closure()

                while finish_count < submit_count:
                    try:
                        r = self._output_queue.get(timeout=MP_STATUS_CHECK_INTERVAL)
                    except queue.Empty:
                        continue

                    index, tensor, name, handle = r

                    # to process split tensor
                    if index >= 0:
                        original_name = self._rev_split_tensor_dict[name]
                        self._split_tensor_to_be_finish[original_name] -= 1
                        self._split_tensor_dict[original_name][index] = tensor

                        if self._split_tensor_to_be_finish[original_name] == 0:
                            tensor = torch.cat(self._split_tensor_dict[original_name], dim=0)
                            name = original_name
                        else:
                            continue

                    p = self._name_parameters.get(name)
                    if handle is not None:
                        tensor = synchronize(handle)

                    self._allreduce_delay[p] = self.backward_passes_per_step
                    p.grad.set_(tensor)

                    d_p = p.grad
                    weight_decay, momentum, dampening, nesterov, lr = self._sgd_parameter[p]
                    if weight_decay != 0:
                        d_p.add_(weight_decay, p.data)
                    if momentum != 0:
                        param_state = self.state[p]
                        if 'momentum_buffer' not in param_state:
                            buf = param_state['momentum_buffer'] = torch.clone(d_p).detach()
                        else:
                            buf = param_state['momentum_buffer']
                            buf.mul_(momentum).add_(1 - dampening, d_p)
                        if nesterov:
                            d_p = d_p.add(momentum, buf)
                        else:
                            d_p = buf
                    p.data.add_(-lr, d_p)
                    finish_count += 1

                self._handles.clear()
                self._split_tensor_dict.clear()
                self._split_tensor_to_be_finish.clear()
                self._rev_split_tensor_dict.clear()
                self._synchronized = False
                return loss
            else:
                self.synchronize()
                self._synchronized = False
                return super(self.__class__, self).step(closure)
        else:
            self._synchronized = False
            return super(self.__class__, self).step(closure)

    def zero_grad(self):
        if self._handles:
            raise AssertionError("optimizer.zero_grad() was called after loss.backward() "
                                 "but before optimizer.step() or optimizer.synchronize(). "
                                 "This is prohibited as it can cause a race condition.")
        return super(self.__class__, self).zero_grad()


class _DistributedAdasumOptimizer(torch.optim.Optimizer):
    def __init__(self, params, named_parameters, compression,
                 backward_passes_per_step=1):
        super(self.__class__, self).__init__(params)

        self._compression = compression

        if named_parameters is not None:
            named_parameters = list(named_parameters)
        else:
            named_parameters = [('allreduce.noname.%s' % i, v)
                                for param_group in self.param_groups
                                for i, v in enumerate(param_group['params'])]

        # make sure that named_parameters are tuples
        if any([not isinstance(p, tuple) for p in named_parameters]):
            raise ValueError('named_parameters should be a sequence of '
                             'tuples (name, parameter), usually produced by '
                             'model.named_parameters().')

        dups = _DistributedOptimizer.find_duplicates([k for k, _ in named_parameters])
        if len(dups) > 0:
            raise ValueError('Parameter names in named_parameters must be unique. '
                             'Found duplicates: %s' % ', '.join(dups))

        all_param_ids = {id(v)
                         for param_group in self.param_groups
                         for v in param_group['params']}
        named_param_ids = {id(v) for k, v in named_parameters}
        unnamed_param_ids = all_param_ids - named_param_ids
        if len(unnamed_param_ids):
            raise ValueError('named_parameters was specified, but one or more model '
                             'parameters were not named. Python object ids: '
                             '%s' % ', '.join(str(id) for id in unnamed_param_ids))

        self._parameter_names = {v: k for k, v in sorted(named_parameters)}
        self.backward_passes_per_step = backward_passes_per_step
        self._allreduce_delay = {v: self.backward_passes_per_step
                                 for _, v in sorted(named_parameters)}
        self._handles = {}
        self._grad_accs = []
        self._requires_update = set()
        self._synchronized = False
        self._should_synchronize = True

        self._starting_models = {
            p : torch.zeros_like(p, requires_grad=False)
            for _, p in named_parameters
        }

        self._register_hooks()

    def set_backward_passes_per_step(self, passes):
        self.backward_passes_per_step = passes
        for p in self._allreduce_delay:
            self._allreduce_delay[p] = self.backward_passes_per_step

    def _register_hooks(self):
        for param_group in self.param_groups:
            for p in param_group['params']:
                if p.requires_grad:
                    p.grad = p.data.new(p.size()).zero_()
                    self._requires_update.add(p)
                    p_tmp = p.expand_as(p)
                    grad_acc = p_tmp.grad_fn.next_functions[0][0]
                    grad_acc.register_hook(self._make_hook(p))
                    self._grad_accs.append(grad_acc)

    def _allreduce_grad_async(self, p):
        # Delta optimizer implements this logic:
        #  start = current.copy()
        #  step() -> computes 'current - \alpha.f(g)' where f is
        #            optimizer logic and g is the gradient
        #  delta = current-start
        #  allreduce_(delta)
        #  start += delta
        #  current = start
        # In order to suppport this logic using function hook to improve performance,
        # we do:
        # delta = (start - \alpha.f(g)) - start
        #       = -\alpha.f(g)
        # set start to zero and step computes -\alpha.f(g)
        # where f is the underlying optimizer logic

        name = self._parameter_names.get(p)
        start = self._starting_models[p]

        stashed_params = []
        for group in self.param_groups:
            stashed_params.append(group['params'])
            # only want to step on p
            if any([p is v for v in group['params']]):
                group['params'] = [p]
            else:
                group['params'] = []

        start.data.copy_(p)

        super(self.__class__, self).step()

        # compute delta = curr - start
        p.data.sub_(start)

        # allreduce as before
        tensor_compressed, ctx = self._compression.compress(p)
        handle = allreduce_async_(tensor_compressed.data, name=name, op=Adasum)

        # reset stashed parameters
        for stashed, group in zip(stashed_params, self.param_groups):
            group['params'] = stashed

        return handle, ctx

    def _make_hook(self, p):
        def hook(*ignore):
            if p in self._handles and self._handles[p][0] is not None:
                if self._allreduce_delay[p] <= 0:
                    raise AssertionError(
                        "Gradients were computed more than "
                        "backward_passes_per_step times before call "
                        "to step(). Increase backward_passes_per_step to "
                        "accumulate gradients locally.")
            assert not p.grad.requires_grad
            assert self._allreduce_delay[p] > 0
            handle, ctx = None, None
            self._allreduce_delay[p] -= 1
            if self._allreduce_delay[p] == 0:
                handle, ctx = self._allreduce_grad_async(p)
            self._handles[p] = (handle, ctx)
        return hook

    def synchronize(self):
        pass

    @contextmanager
    def skip_synchronize(self):
        raise AssertionError("Skipping synchronization is not supported when using Adasum optimizer.")

    def step(self, closure=None):
        loss = None
        if closure is not None:
            loss = closure()

        missing_p = self._requires_update - set(self._handles.keys())
        for p in missing_p:
            handle, ctx = self._allreduce_grad_async(p)
            self._handles[p] = (handle, ctx)

        for p, (handle, ctx) in self._handles.items():
            # This means step() is called before backward_passes_per_steps finished.
            # We do a synchoronous allreduce here.
            if not handle:
                handle, ctx = self._allreduce_grad_async(p)
                self._handles[p] = (handle, ctx)
            delta = synchronize(handle)
            delta = self._compression.decompress(delta, ctx)
            start = self._starting_models[p]
            start.data.add_(delta.data)
            p.data.copy_(start)
            self._allreduce_delay[p] = self.backward_passes_per_step
        self._handles.clear()
        return loss

    def zero_grad(self):
        if self._handles:
            raise AssertionError("optimizer.zero_grad() was called after loss.backward() "
                                 "but before optimizer.step() or optimizer.synchronize(). "
                                 "This is prohibited as it can cause a race condition.")
        return super(self.__class__, self).zero_grad()


def DistributedOptimizer(optimizer, named_parameters=None,
                         compression=Compression.none,
                         backward_passes_per_step=1,
                         op=Average,
                         gradient_predivide_factor=1.0,
                         **kwargs):
    """
    An optimizer that wraps another torch.optim.Optimizer, using an allreduce to
    combine gradient values before applying gradients to model weights.

    Allreduce operations are executed after each gradient is computed by ``loss.backward()``
    in parallel with each other. The ``step()`` method ensures that all allreduce operations are
    finished before applying gradients to the model.

    DistributedOptimizer exposes the ``synchronize()`` method, which forces allreduce operations
    to finish before continuing the execution. It's useful in conjunction with gradient
    clipping, or other operations that modify gradients in place before ``step()`` is executed.
    Make sure to use ``optimizer.skip_synchronize()`` if you're calling ``synchronize()``
    in your code.

    Example of gradient clipping:

    .. code-block:: python

        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.synchronize()
        torch.nn.utils.clip_grad_norm_(model.parameters(), args.clip)
        with optimizer.skip_synchronize():
            optimizer.step()

    Arguments:
        optimizer: Optimizer to use for computing gradients and applying updates.
        named_parameters: A mapping between parameter names and values. Used for naming of
                          allreduce operations. Typically just ``model.named_parameters()``.
        compression: Compression algorithm used during allreduce to reduce the amount
                     of data sent during the each parameter update step.  Defaults to
                     not using compression.
        backward_passes_per_step: Number of expected backward passes to perform
                                  before calling step()/synchronize(). This
                                  allows accumulating gradients over multiple
                                  mini-batches before reducing and applying them.
        op: The reduction operation to use when combining gradients across different ranks.
        gradient_predivide_factor: If op == Average, gradient_predivide_factor splits the averaging
                                   before and after the sum. Gradients are scaled by
                                   1.0 / gradient_predivide_factor before the sum and
                                   gradient_predivide_factor / size after the sum.
    """
    # We dynamically create a new class that inherits from the optimizer that was passed in.
    # The goal is to override the `step()` method with an allreduce implementation.
    if gradient_predivide_factor != 1.0:
        if rocm_built():
            raise ValueError('gradient_predivide_factor not supported yet with ROCm')
        if op != Average:
            raise ValueError('gradient_predivide_factor not supported with op != Average')

    if op != Adasum or size() == 1:
        cls = type(optimizer.__class__.__name__, (optimizer.__class__,),
                   dict(_DistributedOptimizer.__dict__))
        return cls(optimizer.param_groups, named_parameters, compression, backward_passes_per_step, op,
                   gradient_predivide_factor, **kwargs)
    else:
        cls = type(optimizer.__class__.__name__, (optimizer.__class__,),
                   dict(_DistributedAdasumOptimizer.__dict__))
        return cls(optimizer.param_groups, named_parameters, compression, backward_passes_per_step, **kwargs)
