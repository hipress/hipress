# Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

from horovod.common.util import check_extension

check_extension('horovod.mxnet', 'HOROVOD_WITH_MXNET',
                __file__, 'mpi_lib')

from horovod.mxnet.functions import allgather_object, broadcast_object
from horovod.mxnet.mpi_ops import allgather
from horovod.mxnet.mpi_ops import allreduce, allreduce_
from horovod.mxnet.mpi_ops import alltoall
from horovod.mxnet.mpi_ops import broadcast, broadcast_
from horovod.mxnet.mpi_ops import hipress_broadcast_, hipress_gather_,  hipress_reduce_, hipress_alltoall_
from horovod.mxnet.mpi_ops import merge, parse_copyto
from horovod.mxnet.mpi_ops import init, shutdown
from horovod.mxnet.mpi_ops import is_initialized, start_timeline, stop_timeline
from horovod.mxnet.mpi_ops import size, cross_size, local_size, rank, cross_rank, local_rank
from horovod.mxnet.mpi_ops import mpi_threads_supported, mpi_enabled, mpi_built
from horovod.mxnet.mpi_ops import gloo_enabled, gloo_built
from horovod.mxnet.mpi_ops import nccl_built, ddl_built, ccl_built, cuda_built, rocm_built

import mxnet as mx
import types
import warnings
import sys
import time
import math
import os

import zq_tools

def parse_compressed_size(tensor):
    # number of elements in the tensor
    head = tensor[0:4]
    size = tensor[0].asnumpy().item() + tensor[1].asnumpy().item() * 256 + tensor[2].asnumpy().item() * 256 * 256 + tensor[3].asnumpy().item() * 256 * 256 * 256
    return size

# This is where Horovod's DistributedOptimizer wrapper for MXNet goes
class DistributedOptimizer(mx.optimizer.Optimizer):
    def __init__(self, optimizer, gradient_predivide_factor=1.0, comp_alg='', threshold=sys.maxsize, **kwargs):
        if gradient_predivide_factor != 1.0 and rocm_built():
            raise ValueError('gradient_predivide_factor not supported yet with ROCm')

        self._optimizer = optimizer
        # Normalizing rescale_grad by Horovod size, which is equivalent to
        # performing average in allreduce, has better performance.
        self._optimizer.rescale_grad *= (gradient_predivide_factor / size())
        self._gradient_predivide_factor = gradient_predivide_factor
        
        sparsity_algs = ['dgc', 'graddrop']
        quantization_algs = ['tbq', 'ecq', 'terngrad', 'mgc']
        supported_algs = ['dgc']
        
        self._compress = None
        self._decompress = None
        self._residual = False

        self._comp_alg = comp_alg.lower()
        self._threshold = int(threshold * 1024 * 1024)
        self._bits_of_compressed_grad = None
        self._sparsity = True if self._comp_alg in sparsity_algs else False
        self._quantization = True if self._comp_alg in quantization_algs else False
        self._support_compression = True if self._comp_alg in supported_algs else False
        self._compression_rate = 0.0
        
        self._comp_parameters_map = {'tbq':{}, 'terngrad':{}, 'ecq':{}, 'dgc':{}, 'graddrop':{}, 'adacomp':{}, 'mgc':{}}
        self._decomp_parameters_map = {'tbq':{}, 'terngrad':{}, 'ecq':{}, 'dgc':{}, 'graddrop':{}, 'adacomp':{}, 'mgc':{}}
        
        self._comp_gpus = dict() # from keyid to partitioned compressed gradients on gpu
        self._comp_cpus = dict() # from keyid to partitioned compressed gradients on cpu
        self._resd_gpus = dict() # from keyid to partitioned residuals on gpu  
        self._gather_buffer_size = {}
        
        self._turn_root = -1
        self._batch_id = -1
        self._size = size()
        self._rank = rank()
        self._cross_size = cross_size()
        self._cross_rank = cross_rank()
        self._local_size = local_size()
        self._local_rank = local_rank()
        
        if 'log_level' in kwargs.keys():
            self._log_level = kwargs['log_level'] * 10
        else:
            self._log_level = 40
        self._mylog = zq_tools.zq_logger.get_logger("my_log", enable_console=False)
        self._mylog.setLevel(self._log_level) # [1, 5]: DEBUG, INFO, WARNING, ERROR, CRITICAL
        self._mylog.set_rank(self._rank)
        if self._log_level <= 50:
            self._mylog.add_log_file(f"zq_log_rank{self._rank}.log")
        if self._rank == 0:
            self._mylog.info("init optimizer: algorithm={}, log_level={}.".format(self._comp_alg, self._log_level))

        hipress_backend = os.environ.get('HIPRESS_BACKEND', 'NCCL')
        if hipress_backend == 'NCCL':
            self._hipress_backend_level = 0
            self._work_mem = self._comp_gpus
        elif hipress_backend == 'MPI':
            self._hipress_backend_level = 1
            self._work_mem = self._comp_cpus
        self._mylog.info(f"hipress_backend is set to {hipress_backend}")
        
        local_aggregation = os.environ.get('HIPRESS_LOCAL_AGGREGATION', 'False')
        self._local_aggregation = True if local_aggregation == 'True' or local_aggregation == 'true' else False
        self._mylog.info(f"local_aggregation is set to {local_aggregation}")
        # self._comm = 0:GLOBAL, 1:LOCAL, 2:CROSS
        self._comm = 0
        self._comm_rank = self._rank
        self._comm_size = self._size
        
        if self._local_aggregation:
            self._comm = 2
            self._comm_rank = self._cross_rank
            self._comm_size = self._cross_size

        self._expansion_factor = int(os.environ.get('EXPANSION_FACTOR', '1'))
        self._compression_rate = float(os.environ.get('COMPRESSION_RATE', '0.01'))
        self._method = int(os.environ.get('METHOD', '1'))
        self._mylog.info(f"expansion_factor={self._expansion_factor}, compression_rate={self._compression_rate}, method={self._method}")
        
        
        assert(isinstance(comp_alg, str))
        if self._comp_alg == 'dgc':
            self._compress = mx.nd.contrib.dgc_new
            self._static_compress = mx.nd.contrib.dgc_static
            self._decompress = mx.nd.contrib.dgc_new_r
            self._s_percent = self._compression_rate
            self._sample_rate = 0.001
            # DGC compression data store: 
            # |compression data bits| indices| values|
            self._bits_of_compressed_grad = lambda x: (2*math.ceil(x*self._s_percent)+1)*32 # float32 = 32bits
            if 's_percent' in kwargs.keys():
                self._s_percent = kwargs['s_percent']
            if 'sample_rate' in kwargs.keys():
                self._sample_rate = kwargs['sample_rate']
            self._comp_parameters_map['dgc']['s_percent'] = self._s_percent
            self._comp_parameters_map['dgc']['sample_rate'] = self._sample_rate
            self._param_tmp = self._s_percent

    def __getattr__(self, item):
        return getattr(self._optimizer, item)

    def create_state_multi_precision(self, index, weight):
        return self._optimizer.create_state_multi_precision(index, weight)

    def initialize_buffer(self, index, grad, compressed_size, partition_bits=0):
        if self._sparsity:
            # for sparsity, allocate buffer according to compressed_size
            if index in self._gather_buffer_size:
                if self._gather_buffer_size[index] >= compressed_size:
                    return
            self._gather_buffer_size[index] = compressed_size
            self._mylog.info(f"(re)initialize_buffer: batch_id={self._batch_id}, index={index}, new_compressed_size={compressed_size}")
            self._comp_gpus[index] = mx.nd.zeros(shape=compressed_size, dtype="uint8", ctx=grad.context)
            self._comp_cpus[index] = mx.nd.zeros(shape=compressed_size, dtype="uint8", ctx=mx.context.cpu_pinned())
            self._resd_gpus[index] = mx.nd.zeros(shape=grad.size, dtype=grad.dtype, ctx=grad.context)
        else:
            if index in self._gather_buffer_size: 
                if self._gather_buffer_size[index] >= partition_bits: # initialized
                    return    
                elif partition_bits > 0: # Reallocate gather buffer
                    self._gather_buffer_size[index] = partition_bits
                    gather_buffer_size = partition_bits * self._size
                    self._gather_buffer[index] = mx.nd.zeros(shape=gather_buffer_size, dtype="uint8", ctx=grad.context)
            else:
                # 每个grad只分配一个buffer
                self._gather_buffer_size[index] = partition_bits
                if partition_bits > 0:
                    gather_buffer_size = partition_bits * self._size
                    self._gather_buffer[index] = mx.nd.zeros(shape=gather_buffer_size, dtype="uint8", ctx=grad.context)
                self._comp_gpus[index] = mx.nd.zeros(shape=compressed_size, dtype="uint8", ctx=grad.context)
                self._comp_cpus[index] = mx.nd.zeros(shape=compressed_size, dtype="uint8", ctx=mx.context.cpu_pinned())
                self._resd_gpus[index] = mx.nd.zeros(shape=grad.size, dtype=grad.dtype, ctx=grad.context)

    

    def _round_robin_root(self):
        self._turn_root += 1
        if self._local_aggregation:
            self._turn_root %= self._cross_size
        else:
            self._turn_root %= self._size
        return self._turn_root
    
    def _do_compression(self, grad, index, compressed_size, recompress=False):
        self._comp_parameters_map[self._comp_alg]['data'] = grad.reshape(grad.size)
        self._comp_parameters_map[self._comp_alg]['out'] = self._comp_gpus[index]
        self._comp_parameters_map[self._comp_alg]["tensor_index"] = self._batch_id*1000 + index
        if recompress:
            # self._static_compress(**self._comp_parameters_map[self._comp_alg])
            self._comp_parameters_map['dgc']['s_percent'] = self._s_percent * self._comm_size
            self._compress(**self._comp_parameters_map[self._comp_alg])
        else:
            self._comp_parameters_map['dgc']['s_percent'] = self._s_percent
            self._compress(**self._comp_parameters_map[self._comp_alg])

        # if self._residual:
        #     self._comp_parameters_map[self._comp_alg]['residual'] = residual[i]

    def _do_decompression(self, grad, index, start, end):
        self._decomp_parameters_map[self._comp_alg]['data'] = self._comp_gpus[index][start:end]
        self._decomp_parameters_map[self._comp_alg]['is_add_to'] = 1
        self._decomp_parameters_map[self._comp_alg]['out'] = grad.reshape(grad.size)
        self._decomp_parameters_map[self._comp_alg]["tensor_index"] = self._batch_id*1000 +  index
        self._decompress(**self._decomp_parameters_map[self._comp_alg])
    
    def _AllGather(self, grad, index):
        compressed_size = int((self._bits_of_compressed_grad(grad.size))/8) # 1 uint8 = 1 Byte
        root_id = self._round_robin_root()
        self.initialize_buffer(index, grad, compressed_size * self._comm_size)
        if self._comm_rank != root_id:
            self._do_compression(grad, index, compressed_size)
        
        ct_name = str(self._batch_id * 1000 + index)
        if self._hipress_backend_level == 1:
            self._comp_gpus[index][:compressed_size].copyto(self._comp_cpus[index][:compressed_size])
        if self._rank == 0:
            self._mylog.debug(f"{ct_name} compress tensor {compressed_size} * {self._comm_size} byte, buffer {self._work_mem[index].size/1024/1024:.2f}M * {self._work_mem[index].dtype}, grad {grad.size/1024/1024:.2f}M * {grad.dtype}.")
        
        
        # TODO hipress_allgather (non-blocking)

    def _CaSync_sparse(self, grad, index):
        compressed_size = math.ceil((self._bits_of_compressed_grad(grad.size))/8) # 1 uint8 = 1 Byte
        root_id = self._round_robin_root()
        self.initialize_buffer(index, grad, compressed_size * self._comm_size)
        if self._comm_rank != root_id:
            self._do_compression(grad, index, compressed_size)
        
        ct_name = "b"+str(self._batch_id)+"t"+str(index)
        # ct_name = str(self._batch_id * 1000 + index)
        # ct_name = str(index)
        if self._hipress_backend_level == 1:
            self._comp_gpus[index][:compressed_size].copyto(self._comp_cpus[index][:compressed_size])
        if self._rank == 0:
            self._mylog.debug(f"{ct_name} compress tensor {compressed_size} * {self._comm_size} byte, buffer {self._work_mem[index].size/1024/1024:.2f}M * {self._work_mem[index].dtype}, grad {grad.size/1024/1024:.2f}M * {grad.dtype}.")
        
        # self._comp_cpus[index].wait_to_read()
        # size1= parse_compressed_size(self._comp_cpus[index][:compressed_size])
        # self._mylog.debug(f"parse {ct_name} compress tensor {size1} bytes ")
        
        # if root_id != 0 and self._comm_rank == root_id:
        #     offset = self._rank*compressed_size
        #     self._work_mem[index][:compressed_size].copyto(self._work_mem[index][offset:offset+compressed_size])    

        hipress_gather_(self._work_mem[index], grad, root_id, name=ct_name, num_elem=compressed_size, backend=self._hipress_backend_level, comm=self._comm)
        self._mylog.debug("gather {} , root_id={}, {}byte".format(ct_name, root_id, compressed_size))

        if self._comm_rank == root_id:
            # decode gathered compressed tensor
            for idx in range(self._comm_size-1):
                start = idx * compressed_size
                end = start + compressed_size
                if self._hipress_backend_level == 1:
                    self._comp_cpus[index][start:end].copyto(self._comp_gpus[index][start:end])
                    # size1 = parse_compressed_size(self._comp_cpus[index][start:end])
                    # size2 = parse_compressed_size(self._comp_gpus[index][start:end])
                    # self._mylog.debug(f"{ct_name} idx={idx}, compress tensor cpu={size1}, gpu={size2} bytes ")
                self._do_decompression(grad, index, start, end)
            
            self._do_compression(grad, index, compressed_size, recompress=True)
        recompreesd_size = compressed_size*self._expansion_factor
        if self._hipress_backend_level == 1:
            self._comp_gpus[index][:recompreesd_size].copyto(self._comp_cpus[index][:recompreesd_size])

        hipress_broadcast_(self._work_mem[index], root_id, name=ct_name, num_elem=recompreesd_size, backend=self._hipress_backend_level, comm=self._comm)
        self._mylog.debug("broadcast {} , root_id={}, {}byte".format(ct_name, root_id, recompreesd_size))
        if self._comm_rank != root_id:
            if self._hipress_backend_level == 1:
                self._comp_cpus[index][:recompreesd_size].copyto(self._comp_gpus[index][:recompreesd_size])
            # self._do_decompression(grad, index, 0, recompreesd_size)
    
    def _Two_step_allreduce(self, grad, index):
        def _compress_mask(grad, index, mask, compression_rate=0.01, tensor_index=-1):
            compress = mx.nd.contrib.dgc_ta_mask
            compress_config = {}
            compress_config['grad'] = grad
            compress_config['index'] = index
            compress_config['out'] = mask
            compress_config['s_percent'] = compression_rate
            compress_config['tensor_index'] = tensor_index
            compress(**compress_config)

        def _compress_value(grad, index, mask, value):
            compress = mx.nd.contrib.dgc_ta_value
            compress_config = {}
            compress_config['grad'] = grad
            compress_config['mask'] = mask
            compress_config['index'] = index
            compress_config['out'] = value
            compress(**compress_config)

        def _decompress(grad, index, mask, value):
            decompress = mx.nd.contrib.dgc_ta_r
            decompress_config = {}
            decompress_config['mask'] = mask
            decompress_config['index'] = index
            decompress_config['value'] = value
            decompress_config['out'] = grad
            decompress(**decompress_config)
        compressed_size = int(grad.size/8) # 1 uint8 = 1 Byte

        self.initialize_buffer(index, grad, compressed_size)
        comp_index = self._resd_gpus[index][:int(grad.size/2)]
        comp_value = self._resd_gpus[index][int(grad.size/2):]
        mask = self._comp_gpus[index]
        mask_size = int(grad.size / 8 / 4) # TODO
        ct_name = "b"+str(self._batch_id)+"t"+str(index)
        _compress_mask(grad, comp_index, mask, self._compression_rate, tensor_index=1000*self._batch_id+index)
        
        # TODO: allreduce_bor()
        # 现在直接拿allreduce(FP32)代替
        comp_index[0] = mask[0]
        allreduce_(comp_index, average=False, name=ct_name+"_mask", prescale_factor=1.0, num_elem=int(mask_size/4))
        
        _compress_value(grad, comp_index, mask, comp_value)
        
        recompreesd_size = int(grad.size*self._s_percent*self._expansion_factor)
        allreduce_(comp_value, average=False, name=ct_name+"_value", prescale_factor=1.0, num_elem=recompreesd_size)
        
        _decompress(grad, comp_index, mask, comp_value)
        
    
    def _do_allreduce(self, index, grad, name=None):
        if self._size == 1: 
            return
        if not isinstance(index, (tuple, list)): # Bert or other nlp models
            index = [index]
            grad = [grad]

        grad = list(grad)
        for i in range(len(index)):
            grad_shape = grad[i].shape
            grad[i] = grad[i].reshape((-1,))  
            if index[i] == 0:
                self._batch_id += 1

            self._mylog.debug(f"do allreduce batch_id={self._batch_id}, index={index[i]}, grad_size={grad[i].size/1024:.2f}K({grad[i].size/1024/1024:.2f}M).")            
            grad_name = "b"+str(self._batch_id)+"t"+str(index[i])
            # grad_name = str(index[i])
            if self._support_compression and grad[i].size >= self._threshold:
                self._mylog.debug(f"do compress batch_id={self._batch_id}, index={index[i]}, grad_size={grad[i].size/1024/1024:.2f}M.")
                # if self._quantization and compressed_size >=self._min_partition_bits*2: # partition 2 blocks at least
                #     pass
                if self._local_aggregation:
                    self._mylog.debug(f"local aggregation CaSync, batch_id={self._batch_id}, index={index[i]}.")
                    local_reduce_root = 0
                    hipress_reduce_(grad[i], local_reduce_root, name=grad_name, num_elem=grad[i].size)
                    if self._local_rank == local_reduce_root:
                        self._CaSync_sparse(grad[i], index[i])
                    hipress_broadcast_(grad[i], local_reduce_root, name=grad_name, num_elem=grad[i].size, backend=0, comm=1)
                else:
                    # self._mylog.debug(f"global CaSync, batch_id={self._batch_id}, index={index[i]}.")
                    if self._method == 1:
                        self._CaSync_sparse(grad[i], index[i])
                    elif self._method == 2:
                        self._Two_step_allreduce(grad[i], index[i])    
                grad[i].__idiv__(self._size) # average
            else:
                allreduce_(grad[i], average=False, name=grad_name, prescale_factor=1.0 / self._gradient_predivide_factor)
            grad[i].reshape(grad_shape)
                
    def update(self, index, weight, grad, state):
        self._do_allreduce(index, grad)
        self._optimizer.update(index, weight, grad, state)

    def update_multi_precision(self, index, weight, grad, state):
        self._do_allreduce(index, grad)
        self._optimizer.update_multi_precision(index, weight, grad, state)

    def set_learning_rate(self, lr):
        self._optimizer.set_learning_rate(lr)

    def set_lr_mult(self, args_lr_mult):
        self._optimizer.set_lr_mult(args_lr_mult)

    def set_wd_mult(self, args_wd_mult):
        self._optimizer.set_wd_mult(args_wd_mult)


# DistributedTrainer, a subclass of MXNet gluon.Trainer.
# There are two differences between DistributedTrainer and Trainer:
# 1. DistributedTrainer calculates gradients using Horovod allreduce
#    API while Trainer does it using kvstore push/pull APIs;
# 2. DistributedTrainer performs allreduce(summation) and average
#    while Trainer only performs allreduce(summation).
class DistributedTrainer(mx.gluon.Trainer):
    """The distributed trainer for data parallel training.

    Arguments:
        params: dict of parameters to train
        optimizer: mx.optim.Optimizer. the choice of optimizer
        optimizer_params: hyper-parameter of the chosen optimizer
        gradient_predivide_factor: gradient_predivide_factor splits the averaging
              before and after the sum. Gradients are scaled by
              1.0 / gradient_predivide_factor before the sum and
              gradient_predivide_factor / size after the sum.
        prefix: the prefix of the parameters this trainer manages.
              If multiple trainers are used in the same program,
              they must be specified by different prefixes to avoid tensor name collision.
    """
    def __init__(self, params, optimizer, optimizer_params=None,
                 gradient_predivide_factor=1.0, prefix=None):
        if gradient_predivide_factor != 1.0 and rocm_built():
            raise ValueError('gradient_predivide_factor not supported yet with ROCm')
        if isinstance(optimizer, DistributedOptimizer):
            optimizer = optimizer._optimizer
            warnings.warn("DistributedTrainer does not take DistributedOptimizer "
                          "as its optimizer. We have unwrapped it for you.")

        super(DistributedTrainer, self).__init__(
            params, optimizer, optimizer_params=optimizer_params, kvstore=None)

        # _scale is used to check and set rescale_grad for optimizer in Trainer.step()
        # function. Normalizing it by Horovod size, which is equivalent to performing
        # average in allreduce, has better performance. 
        self._scale *= (gradient_predivide_factor / size())
        self._gradient_predivide_factor = gradient_predivide_factor
        assert prefix is None or isinstance(prefix, str)
        self._prefix = prefix if prefix else ""

    def _allreduce_grads(self):
        if size() == 1: return

        # In MXNet 2.0, param.name is no longer unique.
        # Meanwhile, since horovod requires Python 3.6, there is no need to sort
        # self._params as enumerating a python dict is always deterministic.
        for i, param in enumerate(self._params):
            if param.grad_req != 'null':
                allreduce_(param.list_grad()[0], average=False,
                           name=self._prefix + str(i), priority=-i,
                           prescale_factor=1.0 / self._gradient_predivide_factor)


# Wrapper to inject Horovod broadcast after parameter initialization
def _append_broadcast_init(param, root_rank, name):
    init_impl = getattr(param, '_init_impl')
    def wrapped_init_impl(self, *args, **kwargs):
        init_impl(*args, **kwargs)
        broadcast_(self.data(), root_rank=root_rank, name=name)
    return wrapped_init_impl


def broadcast_parameters(params, root_rank=0, prefix=None):
    """Broadcasts the parameters from root rank to all other processes.
    Typical usage is to broadcast the `Module.get_params()` or the
    `Block.collect_params()`.

    Arguments:
        params: One of the following:
            - dict of parameters to broadcast
            - ParameterDict to broadcast
        root_rank: The rank of the process from which parameters will be
                   broadcasted to all other processes.
        prefix: The prefix of the parameters to broadcast.
              If multiple `broadcast_parameters` are called in the same program,
              they must be specified by different prefixes to avoid tensor name collision.
    """
    if size() == 1: return

    tensors = []
    names = []
    assert prefix is None or isinstance(prefix, str)
    prefix = prefix if prefix else ""
    try:
        from mxnet.gluon.parameter import ParameterDict
        valid_types = (dict, ParameterDict)
    except ImportError:
        valid_types = (dict,)
    if isinstance(params, valid_types):
        for name, p in sorted(params.items()):
            try:
                if isinstance(p, mx.gluon.parameter.Parameter):
                    tensors.append(p.data())
                else:
                    tensors.append(p)
                names.append(prefix + str(name))
            except mx.gluon.parameter.DeferredInitializationError:
                # Inject wrapper method with post-initialization broadcast to
                # handle parameters with deferred initialization
                # we use the key of params instead of param.name, since
                # param.name is no longer unique in MXNet 2.0
                new_init = _append_broadcast_init(p, root_rank, prefix + str(name))
                p._init_impl = types.MethodType(new_init, p)
    else:
        raise ValueError('invalid params of type: %s' % type(params))

    # Run broadcasts.
    for tensor, name in zip(tensors, names):
        broadcast_(tensor, root_rank, name=name)
