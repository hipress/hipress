// Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
// Modifications copyright (c) 2020, NVIDIA CORPORATION. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// =============================================================================

#include <atomic>
#include <cuda_runtime.h>
#include <iostream>
#include "../common/operations.h"
#include "cuda_util.h"
#include "mpi_ops.h"

#define unlikely(x) __builtin_expect(!!(x), 0)

namespace horovod {
namespace mxnet {

namespace {

std::atomic_int op_count;

std::string GetOpName(const char* prefix, const char* name) {
  if (name != nullptr) {
    return std::string(prefix) + "." + std::string(name);
  }

  op_count.fetch_add(1);
  return std::string(prefix) + ".noname." + std::to_string(op_count);
}
} // namespace

static const auto MX_EXEC_CTX = Context();
static const auto MX_FUNC_PROP = FnProperty::kCPUPrioritized;
static const char* ALLREDUCE_OP_TYPE_NAME = "horovod_allreduce";
static const char* ALLGATHER_OP_TYPE_NAME = "horovod_allgather";
static const char* BROADCAST_OP_TYPE_NAME = "horovod_broadcast";
static const char* ALLTOALL_OP_TYPE_NAME = "horovod_alltoall";

inline void InvokeCompleteCallback(CallbackOnComplete on_complete, const Status& status) {
  if (status.ok()) {
    on_complete();
  } else {
    auto error = dmlc::Error(status.reason());
    on_complete(&error);
  }
}

inline const char* GetOpTypeName(OperationType op_type) {
  switch (op_type) {
    case OperationType::ALLREDUCE:
      return ALLREDUCE_OP_TYPE_NAME;
    case OperationType::ALLGATHER:
      return ALLGATHER_OP_TYPE_NAME;
    case OperationType::BROADCAST:
      return BROADCAST_OP_TYPE_NAME;
    case OperationType::ALLTOALL:
      return ALLTOALL_OP_TYPE_NAME;
    case OperationType::HIPRESSBROADCAST:
      return "hipress_broadcast";
    case OperationType::HIPRESSGATHER:
      return "hipress_gather";
    case OperationType::HIPRESSREDUCE:
      return "hipress_reduce";
    case OperationType::HIPRESSGRADALLTOALL:
      return "hipress_grad_alltoall";
    case OperationType::HIPRESSGRADALLTOALLBCAST:
      return "hipress_grad_alltoall_bcast";
    default:
      throw std::logic_error("Unsupported Horovod operation type.");
  }
}

inline int GetMyRank(Communicator comm) {
  switch (comm) {
    case Communicator::GLOBAL:
      return horovod_rank();
    case Communicator::CROSS:
      return horovod_cross_rank();
    case Communicator::LOCAL:
      return horovod_local_rank();
    default:
      throw std::logic_error("Communicator type " + CommunicatorName(comm) + " is not supported");
  } 
}

bool IsTensorOnCPU(NDArray* tensor) {
  return tensor->ctx().dev_mask() == cpu::kDevMask;
}

void DoHorovodOperation(void*, void* on_complete_ptr, void* param) {
  ThrowIfError(common::CheckInitialized());

  auto on_complete = *static_cast<CallbackOnComplete*>(on_complete_ptr);
  auto ops_param = static_cast<MpiOpsParam*>(param);
  auto input_tensor = ops_param->input_tensor.get();
  auto output_tensor = ops_param->output_tensor.get();
  auto output = ops_param->output;
  auto name = ops_param->op_name;
  auto average = ops_param->average;
  auto prescale_factor = ops_param->prescale_factor;
  auto postscale_factor = ops_param->postscale_factor;
  auto device = TensorUtil::GetDevice(input_tensor);

  auto hvd_tensor = std::make_shared<MXTensor>(input_tensor);
  auto hvd_context = std::make_shared<MXOpContext>(device, output);  
  std::shared_ptr<Tensor> hvd_output = nullptr;  

  Status enqueue_result;
  switch (ops_param->op_type) {
    case OperationType::ALLREDUCE:
      hvd_output = std::make_shared<MXTensor>(output_tensor);
      enqueue_result = EnqueueTensorAllreduce(
          hvd_context, hvd_tensor, hvd_output, nullptr, name, device,
          [on_complete](const Status& status) {
            InvokeCompleteCallback(on_complete, status);
      }, (average) ? ReduceOp::AVERAGE : ReduceOp::SUM, prescale_factor, postscale_factor, ops_param->num_elem, ops_param->comm);
      break;
    case OperationType::ALLGATHER:
      enqueue_result = EnqueueTensorAllgather(
          hvd_context, hvd_tensor, nullptr, name, device,
          [on_complete](const Status& status) {
            InvokeCompleteCallback(on_complete, status);
      });
      break;
    case OperationType::BROADCAST:
      if (horovod_rank() != ops_param->root_rank) {
        hvd_output = std::make_shared<MXTensor>(output_tensor);
      }

      enqueue_result = EnqueueTensorBroadcast(
          hvd_context, hvd_tensor, hvd_output, ops_param->root_rank,
          nullptr, name, device,
          [on_complete](const Status& status) {
            InvokeCompleteCallback(on_complete, status);
          });
      break;
    case OperationType::ALLTOALL:
    {
      auto hvd_splits = std::make_shared<MXTensor>(ops_param->splits_tensor.get());
      enqueue_result = EnqueueTensorAlltoall(
          hvd_context, hvd_tensor, hvd_splits, nullptr, name, device,
          [on_complete](const Status& status) {
            InvokeCompleteCallback(on_complete, status);
      });
      break;
    }
    case OperationType::HIPRESSBROADCAST:
    {
      int my_rank = GetMyRank(ops_param->comm);
      if (my_rank != ops_param->root_rank) {
        hvd_output = std::make_shared<MXTensor>(output_tensor);
      }
      enqueue_result = EnqueueTensorHipressBroadcast(
      hvd_context, hvd_tensor, hvd_output, ops_param->root_rank,
      nullptr, name, device,
      [on_complete](const Status& status) {
        InvokeCompleteCallback(on_complete, status);
      }, ops_param->num_elem, ops_param->backend, ops_param->comm);
      break;
    }
    case OperationType::HIPRESSGATHER:
    {
      int my_rank = GetMyRank(ops_param->comm);
      if (my_rank == ops_param->root_rank) {
        hvd_output = std::make_shared<MXTensor>(output_tensor);
      }
      enqueue_result = EnqueueTensorHipressGather(
      hvd_context, hvd_tensor, hvd_output, ops_param->root_rank,
      nullptr, name, device,
      [on_complete](const Status& status) {
        InvokeCompleteCallback(on_complete, status);
      }, ops_param->num_elem, ops_param->backend, ops_param->comm);
      break;
    }
    case OperationType::HIPRESSREDUCE:
    {
      hvd_output = std::make_shared<MXTensor>(output_tensor);
      enqueue_result = EnqueueTensorHipressReduce(
      hvd_context, hvd_tensor, hvd_output, ops_param->root_rank,
      nullptr, name, device,
      [on_complete](const Status& status) {
        InvokeCompleteCallback(on_complete, status);
      }, ops_param->num_elem, ops_param->backend, ops_param->comm);
      break;
    }
    case OperationType::HIPRESSGRADALLTOALL:
    {
      hvd_output = std::make_shared<MXTensor>(output_tensor);
      enqueue_result = EnqueueTensorHipressGradAlltoall(
      hvd_context, hvd_tensor, hvd_output, nullptr, name, device,
      [on_complete](const Status& status) {
        InvokeCompleteCallback(on_complete, status);
      }, ops_param->num_elem, ops_param->root_rank,
      ops_param->backend, ops_param->comm);
      break;
    }
    case OperationType::HIPRESSGRADALLTOALLBCAST:
    {
      hvd_output = std::make_shared<MXTensor>(output_tensor);
      enqueue_result = EnqueueTensorHipressGradAlltoallBcast(
      hvd_context, hvd_tensor, hvd_output, nullptr, name, device,
      [on_complete](const Status& status) {
        InvokeCompleteCallback(on_complete, status);
      }, ops_param->num_elem, ops_param->root_rank,
      ops_param->backend, ops_param->comm);
      break;
    }
    default:
    {
      throw std::logic_error("Unsupported Horovod operation type.");
    }
  }

  ThrowIfError(enqueue_result);
}

inline void PushHorovodOperation(OperationType op_type, NDArray* input,
                                 NDArray* output, const char* name,
                                 int priority, int root_rank = -1,
                                 bool average = true,
                                 NDArray* splits = nullptr,
                                 double prescale_factor = 1.0,
                                 double postscale_factor = 1.0,
                                 NDArray* depend = nullptr,
                                 int num_elem = -1,
                                 CommnicationBackend backend = CommnicationBackend::NCCL,
                                 Communicator comm = Communicator::GLOBAL) {
  auto op_type_name = GetOpTypeName(op_type);
  auto op_name = CommunicatorName(comm) + "." + GetOpName(op_type_name, name);

  // We need to create a shared_ptr to NDArray object with
  // shallow copy to prevent from NDArray object being freed
  // before MXNet engine process it
  auto input_copy = std::make_shared<NDArray>(*input);
  auto output_copy = std::make_shared<NDArray>(*output);
  std::shared_ptr<NDArray> splits_tensor;
  if (splits) {
#if HAVE_CUDA
    // We expect splits to be a tensor on CPU. Create CPU copy if required.
    if (!IsTensorOnCPU(splits)) {
      splits_tensor = std::make_shared<NDArray>(Context::Create(Context::kCPU, 0),
      splits->dtype());
      TensorUtil::AsyncCopyCudaToCPU(splits, splits_tensor.get());
    } else {
      splits_tensor = std::make_shared<NDArray>(*splits);
    }
#else
    splits_tensor = std::make_shared<NDArray>(*splits);
#endif
  }
  auto ops_param = CreateMpiOpsParam(input_copy, output_copy, output,
    nullptr /* cpu_input_tensor */, nullptr /* cpu_output_tensor */,
    op_type, op_name, root_rank, average, splits_tensor, prescale_factor, postscale_factor,
    num_elem, backend, comm);

  // Not in-place
  auto input_var = input->var();
  auto output_var = output->var();
  if (input_var != output_var) {
    std::vector<void*> input_vars {input_var};
    if (splits) {
      // Add splits tensor to input list to enforce dependency on possible async D2H copy
      input_vars.push_back(splits_tensor->var());
    }
    MXEnginePushAsync(DoHorovodOperation, ops_param, DeleteMpiOpsParam,
                      &MX_EXEC_CTX, input_vars.data(), input_vars.size(), &output_var, 1,
                      &MX_FUNC_PROP, priority, op_type_name);
  // In-place
  } else {
    std::vector<void*> input_vars;
    if (splits) {
      input_vars.push_back(splits_tensor->var());
    }
    if (depend) {
      input_vars.push_back(depend->var());
    }
    MXEnginePushAsync(DoHorovodOperation, ops_param, DeleteMpiOpsParam,
                      &MX_EXEC_CTX, input_vars.data(), input_vars.size(), &output_var, 1,
                      &MX_FUNC_PROP, priority, op_type_name);
  }
}

#if HAVE_CUDA
void DoHorovodOperationCudaOnCPU(void*, void* on_complete_ptr, void* param) {
  ThrowIfError(common::CheckInitialized());

  auto on_complete = *static_cast<CallbackOnComplete*>(on_complete_ptr);
  auto ops_param = static_cast<MpiOpsParam*>(param);
  auto name = ops_param->op_name;
  auto hvd_cpu_buffer = std::make_shared<MXTensor>(ops_param->cpu_input_tensor.get());
  auto hvd_context = std::make_shared<MXOpContext>(
    CPU_DEVICE_ID, ops_param->cpu_output_tensor.get());
  auto average = ops_param->average;
  auto prescale_factor = ops_param->prescale_factor;
  auto postscale_factor = ops_param->postscale_factor;

  Status enqueue_result;
  switch (ops_param->op_type) {
    case OperationType::ALLREDUCE:
      enqueue_result = EnqueueTensorAllreduce(
          hvd_context, hvd_cpu_buffer, hvd_cpu_buffer, nullptr, name, CPU_DEVICE_ID,
          [on_complete](const Status& status) {
            InvokeCompleteCallback(on_complete, status);
      }, (average) ? ReduceOp::AVERAGE : ReduceOp::SUM, prescale_factor, postscale_factor);
      break;
    case OperationType::ALLGATHER:
      enqueue_result = EnqueueTensorAllgather(
          hvd_context, hvd_cpu_buffer, nullptr, name, CPU_DEVICE_ID,
          [on_complete](const Status& status) {
            InvokeCompleteCallback(on_complete, status);
      });
      break;
    case OperationType::BROADCAST:
      enqueue_result = EnqueueTensorBroadcast(
          hvd_context, hvd_cpu_buffer, hvd_cpu_buffer, ops_param->root_rank,
          nullptr, name, CPU_DEVICE_ID,
          [on_complete](const Status& status) {
            InvokeCompleteCallback(on_complete, status);
      });
      break;
    case OperationType::ALLTOALL:
    {
      auto hvd_splits = std::make_shared<MXTensor>(ops_param->splits_tensor.get());
      enqueue_result = EnqueueTensorAlltoall(
          hvd_context, hvd_cpu_buffer, hvd_splits, nullptr, name, CPU_DEVICE_ID,
          [on_complete](const Status& status) {
            InvokeCompleteCallback(on_complete, status);
      });
      break;
    }
    default:
      throw std::logic_error("Unsupported Horovod operation type.");
  }

  ThrowIfError(enqueue_result);
}

inline void PushHorovodOperationCudaOnCPU(OperationType op_type, NDArray* input,
                                          NDArray* output, const char* name,
                                          int priority, int root_rank = -1,
                                          bool average = true,
                                          NDArray* splits = nullptr,
                                          double prescale_factor = 1.0,
                                          double postscale_factor = 1.0,
                                          int num_elem = -1,
                                          CommnicationBackend backend = CommnicationBackend::NCCL,
                                          Communicator comm = Communicator::GLOBAL) {
  auto op_type_name = GetOpTypeName(op_type);
  auto op_name = GetOpName(op_type_name, name);

  auto cpu_input_tensor = std::make_shared<NDArray>(Context::Create(Context::kCPU, 0),
    input->dtype());
  auto cpu_output_tensor = std::make_shared<NDArray>(Context::Create(Context::kCPU, 0),
    input->dtype());

  // Make async copy of input tensor to CPU tensor.
  TensorUtil::AsyncCopyCudaToCPU(input, cpu_input_tensor.get());

  std::shared_ptr<NDArray> splits_tensor;
  if (splits) {
    // We expect splits to be a tensor on CPU. Create CPU copy if required.
    if (!IsTensorOnCPU(splits)) {
      splits_tensor = std::make_shared<NDArray>(Context::Create(Context::kCPU, 0),
      splits->dtype());
      TensorUtil::AsyncCopyCudaToCPU(splits, splits_tensor.get());
    } else {
      splits_tensor = std::make_shared<NDArray>(*splits);
    }
  }

  auto ops_param = CreateMpiOpsParam(nullptr, nullptr, output, cpu_input_tensor,
                                     cpu_output_tensor, op_type, op_name, root_rank,
                                     average, splits_tensor, prescale_factor, postscale_factor);

  auto input_var = input->var();
  auto output_var = output->var();
  auto cpu_input_var = cpu_input_tensor->var();
  auto cpu_output_var = cpu_output_tensor->var();
  if (op_type == OperationType::ALLGATHER ||
      op_type == OperationType::ALLTOALL) {
    // Use out-of-place path for operations that have unknown output size (allgather, alltoall)
    std::vector<void*> input_vars {cpu_input_var};
    if (splits) {
      // Add splits tensor to input list to enforce dependency on possible async D2H copy
      input_vars.push_back(splits_tensor->var());
    }

    MXEnginePushAsync(DoHorovodOperationCudaOnCPU, ops_param, DeleteMpiOpsParam,
                      &MX_EXEC_CTX, input_vars.data(), input_vars.size(), &cpu_output_var, 1,
                      &MX_FUNC_PROP, priority, op_type_name);

    // Since cpu_output_tensor is resized in out-of-place path, need
    // to wait for operation to complete before copying to GPU output.
    cpu_output_tensor->WaitToRead();

    // Make async copy of CPU output tensor to output tensor.
    TensorUtil::AsyncCopyCPUToCuda(cpu_output_tensor.get(), output);
  } else {
    // Use in-place otherwise
    MXEnginePushAsync(DoHorovodOperationCudaOnCPU, ops_param, DeleteMpiOpsParam,
                      &MX_EXEC_CTX, nullptr, 0, &cpu_input_var, 1,
                      &MX_FUNC_PROP, priority, op_type_name);

    // Make async copy of CPU input tensor to output tensor.
    TensorUtil::AsyncCopyCPUToCuda(cpu_input_tensor.get(), output);
  }
}
#endif

extern "C" int horovod_mxnet_allreduce_async(NDArray* input, NDArray* output,
                                             const char* name, bool average,
                                             int priority,
                                             double prescale_factor,
                                             double postscale_factor,
                                             int num_elem,
                                             Communicator comm) {
  MX_API_BEGIN();

#if HAVE_ROCM
  // Averaging left at framework level for ROCm until ScaleBuffer implementation
  // added.
  bool average_in_framework = average;
  average = false;
#endif

#if HAVE_CUDA && !HOROVOD_GPU_ALLREDUCE
  if (IsTensorOnCPU(input) && IsTensorOnCPU(output)) {
    PushHorovodOperation(OperationType::ALLREDUCE, input, output,
                         name, priority, -1, average, nullptr, prescale_factor, postscale_factor, nullptr, num_elem);
  } else {
    PushHorovodOperationCudaOnCPU(OperationType::ALLREDUCE, input, output,
                                  name, priority, -1, average, nullptr, prescale_factor, postscale_factor);
  }
#else
  PushHorovodOperation(OperationType::ALLREDUCE, input, output,
                       name, priority, -1, average, nullptr, prescale_factor, postscale_factor, nullptr, num_elem);
#endif

#if HAVE_ROCM
  if (average_in_framework) {
    *output /= horovod_size();
  }
#endif

  MX_API_END();
}

extern "C" int horovod_mxnet_allgather_async(NDArray* input,
                                             NDArray* output,
                                             const char* name, int priority) {
  MX_API_BEGIN();

#if HAVE_CUDA && !HOROVOD_GPU_ALLGATHER
  if (IsTensorOnCPU(input) && IsTensorOnCPU(output)) {
    PushHorovodOperation(OperationType::ALLGATHER, input, output,
                         name, priority);
  } else {
    PushHorovodOperationCudaOnCPU(OperationType::ALLGATHER, input, output,
                                  name, priority);
  }
#else
  PushHorovodOperation(OperationType::ALLGATHER, input, output,
                       name, priority);
#endif

  MX_API_END();
}

extern "C" int horovod_mxnet_broadcast_async(NDArray* input,
                                             NDArray* output,
                                             const char* name, int root_rank,
                                             int priority) {
  MX_API_BEGIN();

#if HAVE_CUDA && !HOROVOD_GPU_BROADCAST
  if (IsTensorOnCPU(input) && IsTensorOnCPU(output)) {
    PushHorovodOperation(OperationType::BROADCAST, input, output,
                         name, priority, root_rank);

  } else {
    PushHorovodOperationCudaOnCPU(OperationType::BROADCAST, input, output,
                                  name, priority, root_rank);
  }
#else
  PushHorovodOperation(OperationType::BROADCAST, input, output,
                       name, priority, root_rank);
#endif

  MX_API_END();
}

extern "C" int horovod_mxnet_alltoall_async(NDArray* input,
                                            NDArray* output,
                                            const char* name,
                                            NDArray* splits,
                                            int priority) {
  MX_API_BEGIN();

#if HAVE_CUDA && !HOROVOD_GPU_ALLTOALL
  if (IsTensorOnCPU(input) && IsTensorOnCPU(output)) {
    PushHorovodOperation(OperationType::ALLTOALL, input, output,
                         name, priority, -1, false, splits);

  } else {
    PushHorovodOperationCudaOnCPU(OperationType::ALLTOALL, input, output,
                                  name, priority, -1, false, splits);
  }
#else
  PushHorovodOperation(OperationType::ALLTOALL, input, output,
                       name, priority, -1, false, splits);
#endif

  MX_API_END();
}

int32_t DoParse(NDArray* tensor, bool in_cpu){
  void* ct_vp = static_cast<void*>(tensor->data().dptr<uint8_t>());
  int32_t* ct_ip = static_cast<int32_t*>(ct_vp);
  int32_t msg_size;
  if (in_cpu) {
    msg_size = ct_ip[0];
  } else {
    cudaMemcpy(&msg_size, ct_vp, sizeof(int32_t), cudaMemcpyDeviceToHost);
  } 
  return msg_size;
}

void ErrorCheck(std::string op_name, cudaError_t cuda_result) {
  if (cuda_result != cudaSuccess) {
    throw std::logic_error(std::string(op_name) + " failed: " + cudaGetErrorString(cuda_result));
  }
}

void DoParseCopyto(NDArray* src, NDArray* dst, int mode, CallbackOnComplete on_complete) {
  void* src_vp = static_cast<void*>(src->data().dptr<uint8_t>());
  void* dst_vp = static_cast<void*>(dst->data().dptr<uint8_t>());
  int32_t msg_size = DoParse(src, mode == 1);

  switch(mode) {
    case 1:
      ErrorCheck("cudaMemcpy", cudaMemcpy(dst_vp, src_vp, msg_size, cudaMemcpyHostToDevice));
      break;
    case 2:
      ErrorCheck("cudaMemcpy", cudaMemcpy(dst_vp, src_vp, msg_size, cudaMemcpyDeviceToHost));
      break;
    default:
      throw std::logic_error("Invalid mode");
  }
  on_complete();
}

extern "C" int horovod_mxnet_parse_copyto(NDArray* src, NDArray* dst, int mode) {
  MX_API_BEGIN();
  auto parse_copyto_fn = [src, dst, mode]
                        (RunContext rctx, CallbackOnComplete on_complete) mutable {
    DoParseCopyto(src, dst, mode, on_complete);
  };
  
  Engine::Get()->PushAsync(parse_copyto_fn, src->ctx(),
                             {src->var()}, {dst->var()},
                             FnProperty::kNormal, 0, "ParseCopyto");
  MX_API_END();
}

extern "C" int horovod_mxnet_hipress_reduce_async(NDArray* input,
                                                NDArray* output,
                                                const char* name,
                                                int num_elem,
                                                CommnicationBackend backend,
                                                Communicator comm,
                                                int root_rank, int priority) {
  MX_API_BEGIN();

  PushHorovodOperation(OperationType::HIPRESSREDUCE, input, output,
                         name, priority, root_rank, true, nullptr, 1.0, 1.0, nullptr,
                         num_elem, backend, comm);

  MX_API_END();
}

extern "C" int horovod_mxnet_hipress_alltoall_async(NDArray* input,
                                                NDArray* output,
                                                const char* name,
                                                int num_elem,
                                                CommnicationBackend backend,
                                                Communicator comm,
                                                int root_rank, int priority) {
  MX_API_BEGIN();
  PushHorovodOperation(OperationType::HIPRESSGRADALLTOALL, input, output,
                         name, priority, root_rank, true, nullptr, 1.0, 1.0, nullptr,
                         num_elem, backend, comm);
  MX_API_END();
}

extern "C" int horovod_mxnet_hipress_alltoall_bcast_async(NDArray* input,
                                                NDArray* output,
                                                const char* name,
                                                int num_elem,
                                                CommnicationBackend backend,
                                                Communicator comm,
                                                int root_rank, int priority) {
  MX_API_BEGIN();
  PushHorovodOperation(OperationType::HIPRESSGRADALLTOALLBCAST, input, output,
                         name, priority, root_rank, true, nullptr, 1.0, 1.0, nullptr,
                         num_elem, backend, comm);
  MX_API_END();
}

extern "C" int horovod_mxnet_hipress_gather_async(NDArray* input,
                                             NDArray* output,
                                             NDArray* depend,
                                             const char* name,
                                             int num_elem,
                                             CommnicationBackend backend,
                                             Communicator comm,
                                             int root_rank, 
                                             int priority
                                             ) {
  MX_API_BEGIN();
  PushHorovodOperation(OperationType::HIPRESSGATHER, input, output,
                         name, priority, root_rank, true, nullptr, 1.0, 1.0, depend, num_elem, backend, comm);
                         

  MX_API_END();
}

extern "C" int horovod_mxnet_hipress_broadcast_async(NDArray* input,
                                                NDArray* output,
                                                const char* name,
                                                int num_elem,
                                                CommnicationBackend backend,
                                                Communicator comm,
                                                int root_rank, 
                                                int priority
                                                ) {
  MX_API_BEGIN();
  
  PushHorovodOperation(OperationType::HIPRESSBROADCAST, input, output,
                         name, priority, root_rank, true, nullptr, 1.0, 1.0, nullptr,
                         num_elem, backend, comm);

  MX_API_END();
}

void two_tensor_merge(int32_t* tensor1, int32_t* tensor2, int32_t* output_tensor){
  int32_t num_of_pairs1 = (tensor1[0]-4)/2/4;
  int32_t* index_pointer1 = tensor1 + 1;
  float* value_pointer1 = (float*)(index_pointer1 + num_of_pairs1);
  int32_t num_of_pairs2 = (tensor2[0]-4)/2/4;
  int32_t* index_pointer2 = tensor2 + 1;
  float* value_pointer2 = (float*)(index_pointer2 + num_of_pairs2);
  int i=0, j=0, k=0;
  int32_t* temp_indices = new int32_t[num_of_pairs1+num_of_pairs2+1];
  float* temp_values = new float[num_of_pairs1+num_of_pairs2+1];
  // printf("ct_a=%dbytes, ct_b=%d\n", tensor1[0], tensor2[0]);
  if (unlikely(num_of_pairs1 == 0 || num_of_pairs2 == 0)) {
    std::cout << "WARNING: ct_a="<< tensor1[0] << "bytes, ct_b=" << tensor2[0] << "bytes" << std::endl;
  }
  while(i<num_of_pairs1 && j<num_of_pairs2){
    if(index_pointer1[i] == index_pointer2[j]){
      temp_indices[k] = index_pointer1[i];
      temp_values[k] = value_pointer1[i] + value_pointer2[j];
      i++;
      j++;
      k++;
    }
    else if(index_pointer1[i] < index_pointer2[j]){
      temp_indices[k] = index_pointer1[i];
      temp_values[k] = value_pointer1[i];
      i++;
      k++;
    }
    else{
      temp_indices[k] = index_pointer2[j];
      temp_values[k] = value_pointer2[j];
      j++;
      k++;
    }
  }
  if (i < num_of_pairs1){
    for (int l = i; l < num_of_pairs1; l++){
      temp_indices[k] = index_pointer1[l];
      temp_values[k] = value_pointer1[l];
      k++;
    }
  }
  if (j < num_of_pairs2){
    for (int l = j; l < num_of_pairs2; l++){
      temp_indices[k] = index_pointer2[l];
      temp_values[k] = value_pointer2[l];
      k++;
    }
  }
  output_tensor[0] = k*2*4+4;
  //std::cout << "Merge(" << tensor1[0] << ", " << tensor2[0] << ") = " << output_tensor[0] << std::endl;
  memcpy(output_tensor+1, temp_indices, k*sizeof(int32_t));
  memcpy(output_tensor+1+k, temp_values, k*sizeof(float));
  delete [] temp_indices;
  delete [] temp_values;
  //return output[0]; // Bytes
}

void DoMerge(NDArray* tensor, int partition_num, int partition_size, CallbackOnComplete on_complete){
  void* ct_vp = static_cast<void*>(tensor->data().dptr<uint8_t>());
  int32_t* ct_ip = static_cast<int32_t*>(ct_vp);

  int idx = 1;
  while (idx < partition_num){
    int32_t* tensor1 = ct_ip;
    void* ct2_vp = ct_vp + idx * partition_size;
    int32_t* tensor2 = static_cast<int32_t*>(ct2_vp);
    two_tensor_merge(tensor1, tensor2, tensor1);
    idx++;
  }
  on_complete();
}

extern "C" int horovod_mxnet_merge(NDArray* tensor, int partition_num, int partition_size){
  MX_API_BEGIN();

  auto sparse_reduce_fn = [tensor, partition_num, partition_size] (RunContext rctx,
                             CallbackOnComplete on_complete) mutable{
    DoMerge(tensor, partition_num, partition_size, on_complete);
  };
  
  Engine::Get()->PushAsync(sparse_reduce_fn,
                        tensor->ctx(), 
                        {},
                        {tensor->var()},
                        FnProperty::kNormal,
                        0,
                        "SparseReduceOnCPU");
  MX_API_END();
}

} // namespace mxnet
} // namespace horovod
