# cd /data/horovod/; bash install.sh
# IP_LIST=(gpu3 gpu4 gpu5 gpu6 gpu8 gpu9 gpu10 gpu11) 45 44 50 51 46 48 49
IP_LIST=(gpu5 gpu6 gpu10 gpu11 gpu8)
# disable gpu2 gpu7
NODES=4
PORT=12123

# local
# bash install.sh
# cp horovod/mxnet/__init__.py /usr/local/lib/python3.8/dist-packages/horovod-0.20.3-py3.8-linux-x86_64.egg/horovod/mxnet/__init__.py 


# remote
for(( i=0;i<${NODES};i++))
do
    ssh -p ${PORT} ${IP_LIST[i]} "cd /data/hipress/CCO; bash install.sh"
    # ssh -o "StrictHostKeyChecking no" -p ${PORT} ${IP_LIST[i]}  "cp /data/hipress/CCO/horovod/mxnet/__init__.py /usr/local/lib/python3.8/dist-packages/horovod-0.20.3-py3.8-linux-x86_64.egg/horovod/mxnet/__init__.py "
    # ssh -p ${PORT} ${IP_LIST[i]} "pip install gluonnlp"
    # ssh -p ${PORT} ${IP_LIST[i]} "cd /data/hipress/deps/mxnet-1.9.0/python; pip install -e ."
    echo "ssh ${IP_LIST[i]} done"
done